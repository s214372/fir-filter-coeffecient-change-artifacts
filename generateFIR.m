%Generate minimum-phase FIR coefficients

%{
Fs = 48000;
order = 256;
lambda = 0.75;
warped = 1;
scale1 = 1;
scale2 = 1;
%}

oversample = 4;
order = order*oversample;

%Set frequency domain characteristics
points1 = [[0; 0.01] [0.17; 0.1] [0.19; 20] [0.22; 20] [0.23; 20] [0.25; 0.1] [0.3; 0.01] [0.3; 2000] [0.4; 20] [0.45; 0.001] [0.55; 0.000001] [0.6; 5] [0.65; 100] [0.65; 1] [0.7; 0.001] [0.7; 0.01] [0.74; 0.01] [0.74; 50] [0.83; 20] [0.85; 20] [0.88; 0.01] [0.9; 0] [1; 0]];
points2 = [[0; 0.01] [0.05; 0.0001] [0.12; 0.00001] [0.22; 0.00001] [0.23; 0.001] [0.27; 50] [0.3; 5] [0.3; 0.000001] [0.4; 0.1] [0.45; 2000] [0.45; 20] [0.55; 20] [0.6; 5] [0.65; 0.01] [0.65; 1] [0.7; 0.001] [0.7; 0.01] [0.74; 0.001] [0.74; 0.00001] [0.85; 0.00001] [0.88; 0.0001] [0.9; 10] [0.95; 0] [1; 0]];

if warped
    %Warp frequency scale
    f1 = points1(1,:)*pi;
    fw1 = atan(((1-lambda^2)*sin(f1))./((1+lambda^2)*cos(f1)-2*lambda));
    fw1 = fw1/pi - floor(fw1/pi);
    
    f2 = points2(1,:)*pi;
    fw2 = atan(((1-lambda^2)*sin(f2))./((1+lambda^2)*cos(f2)-2*lambda));
    fw2 = fw2/pi - floor(fw2/pi);
else
    fw1 = points1(1,:);
    fw2 = points2(1,:);
end

%Convert to FIR
b1 = fir2(order-1, fw1, points1(2,:));
b2 = fir2(order-1, fw2, points2(2,:));

w = [1 2*ones(1,order/2-1) ones(1,1-rem(order,2)) zeros(1,order/2-1)];
w = w.*fftshift(transpose(hamming(order)));

order = order/oversample;

y1 = real(ifft(log(abs(fft(b1)))));
yw1 = exp(fft((w).*y1));
f1 = zeros(1, order);
for i = 1:order
    f1(i) = mean(yw1((i*4-3):1:(i*4)));
end
b1 = real(ifft(f1))/scale1;

y2 = real(ifft(log(abs(fft(b2)))));
yw2 = exp(fft((w).*y2));
f2 = zeros(1, order);
for i = 1:order
    f2(i) = mean(yw2((i*4-3):1:(i*4)));
end
b2 = real(ifft(f2))/scale2;

%
clear oversample
%clear points1
%clear points2
clear i
clear f1
clear f2
clear fw1
clear fw2
clear w
clear y1
clear y2
clear yw1
clear yw2
%}
%{
close all force
a = 1;

isminphase(b1,a)
isminphase(b2,a)

figure(1)
hold on;
plot(points1(1,:),points1(2,:))
plot(points2(1,:),points2(2,:))
hold off;
set(gca, 'YScale', 'log')
set(gcf,'position',[100,500,500,500])

figure(2)
zplane(b1, a)
set(gcf,'position',[10,100,300,300])

figure(3)
zplane(b2, a)
set(gcf,'position',[325,100,300,300])

figure(4)
freqz(b1, a, order, 48000)
set(gcf,'position',[650,550,600,400])

figure(5)
freqz(b2, a, order, 48000)
set(gcf,'position',[650,50,600,400])

figure(6)
hold on;
impres = filter(b1,a,[1 zeros(1,order-1)]);
stem(impres)
impres = filter(b2,a,[1 zeros(1,order-1)]);
stem(impres)
hold off;
set(gcf,'position',[1300,400,500,500])
%}