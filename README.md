
# FIR Filter Coeffecient Change Artifacts

This repository contains all the files used for the bachelor project "Analyzing Variable FIR Filter Coefficient-Change Artifacts and Implementing Hardware to Counteract Them" by Simon Ravn, as documented by the report PDF in [/Report](Report/).

Available here are all Matlab scripts used for simulation and analysis in the project, Vivado projects used for creating and implementing a filtering system in FPGA hardware, the python script used for testing that hardware, and all graph source files for the illustrations in [/Diagrams](Diagrams/).

Additionally, a clone of the GstPEAQ repo is included within this project in [/gstpeaq-develop github](gstpeaq-develop%20github/): [GitHub link](https://github.com/HSU-ANT/gstpeaq). This clone was pulled on the 12th of March 2024 and has only been modified to be executable, plus a library of audio files was added.

## Description

The other subdirectories contain the following:
- [/Articles](Articles/) : PDFs of various sources used throughout the project.
- [/Diagrams](Diagrams/) : All the graphs and illustrations generated for the project.
- [/Hardware Files](Hardware%20Files/) : Various files used for hardware development, including Excel based code generators, draw.io diagram files and a folder with .zip files of Vivado projects.
- [/Test Data](Test%20Data/) : A dump for various data files used to store and pass data from simulations.
&nbsp;

The Matlab scripts were used (and can be re-run) for the following:

- PEAQ
  - peaq.m and peaqScript.m run the basic GstPEAQ C script in Matlab (cmds.txt is also a small note of the console commands used to call GstPEAQ).
  - peaqTest.m and peaqTestFFTs.m test the GstPEAQ implementation.
&nbsp;

- Filter Simulations and Tests
  - generateFIR.m generates two sets of coefficients used by the following simulations.
  - simFIR.m and simWFIR.m simulate the canonical, transposed, and lattice architechtures for normal and warped FIR filters in hardware with a tone complex input signal and coefficient change occuring during filtering.
  - simSpectrogram.m simulates the canonical and transposed FIR and WFIR structures from the previous simulations with a pulse-based coefficient change.
&nbsp;

- Simulations for Comparison with Hardware Results
  - (filterPCIE_runTest.py is the python script used to run tests on a PC with the FPGA hardware installed and programmed).
  - simWFIR_hardwareSim.m simulates the canonical and transposed FIR and WFIR structures with implicit fixed-point 32-bit values and given scaling, plus comparison with hardware results from the same tests of the same structures.
  - hardwarePEAQ.m and hardwarePEAQ_fixedHardware.m simulates the canonical and transposed FIR and WFIR structures for comparison with hardware with and without a full fixed-point implementation, all tested with audio files.
&nbsp;

- Simulations of Methods to Counteract Found Artifacts
  - simFIR_FixedTrans.m simulates two methods of counteracting the mixed-coefficients artifact found in the transposed FIR architecture.
  - simWFIR_FixedStep.m simulates an interpolation-based method of counteracting the frequency-step artifact found, in the canonical WFIR architecture.
&nbsp;

## License
Copyright (c) 2024 Simon Steen Ravn

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
