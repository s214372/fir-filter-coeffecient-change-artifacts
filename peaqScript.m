%runs peaq script on a ref and test file

%% Reqiured Input:

%Reference file for peaq : string
%refFile = 'gstpeaq-develop github\vs\x64\Release\sounds\piano.wav';

%Test file for peaq : string
%testFile = 'gstpeaq-develop github\vs\x64\Release\sounds\note.wav';

%% Expected Output:
% PEAQversion : string
% objectiveDifferenceGrade : double
% distortionIndex : double

%% Call peaq script version
command = "powershell;./'gstpeaq-develop github\vs\x64\Release\peaq' --version";
[~,cmdout] = system(command);

%Identify version text in cmdout as initial text up until "Copyright "
PEAQversion = '';
compStr = 'Copyright ';
for i = 10:(length(cmdout)-length(compStr))
  if strcmp(cmdout(i:(i+length(compStr)-1)),compStr)
    PEAQversion = cmdout(1:i-2);
    break
  end
end

%Assert error if wrong version
if not(strcmp(PEAQversion,'GstPEAQ 0.6.1'))
    error('Version Mismatch');
end

%% Call basic peaq script
command = "powershell;./'gstpeaq-develop github\vs\x64\Release\peaq' --basic";
[~,cmdout] = system(append(command," '",refFile,"' '",testFile,"'"));

%Identify doubles in cmdout from string fields other than the initial
%"Objective Difference Grade: " and middle "Distortion Index: " strings
objectiveDifferenceGrade = NaN;
distortionIndex = NaN;
compStr1 = 'Objective Difference Grade: ';
compStr2 = 'Distortion Index: ';
for i = (length(compStr1)+3):(length(cmdout)-length(compStr2))
  if strcmp(cmdout(i:(i+length(compStr2)-1)),compStr2)
    objectiveDifferenceGrade = str2double(cmdout(length(compStr1):i-2));
    distortionIndex = str2double(cmdout(i+length(compStr2):end));
    break
  end
end



clear i
clear compStr
clear compStr1
clear compStr2
