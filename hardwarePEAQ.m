%Filtering hardware simulations for comparison with filtering hardware implementations with implicit (21,10) fixed-point values, tested with audio files as input

close all force
clear

fprintf('Start Time: %s\n', datetime("now"));

%% Basic Setup
file = 1;
if file == 1
    file = 'piano';
    scale1 = 4;
    scale2 = 1;
    fileOffset = 224000;
end
if file == 2
    file = 'rock';
    scale1 = 192;
    scale2 = 96;
    fileOffset = 638000;
end
if file == 3
    file = 'orchestra';
    scale1 = 32;
    scale2 = 16;
    fileOffset = 183000;
end
order = 256;
fileLength = 56000;
change = int32(0x40);
intScale = 1024;


refFile = strcat('gstpeaq-develop github\vs\x64\Release\sounds\', file, '.wav');

[refData,Fs] = audioread(refFile);
refDataFull = refData;
refData = refData(fileOffset:fileOffset+fileLength-1);
%{
fileRefData = fopen('ref_data.txt','w');
fprintf(fileRefData,'%d\n',round(refData*intScale,0));
fclose(fileRefData);
%}
if length(refData) > 0x800
    change = change*int32(0x80);
end



%% Standard FIR Setup
warped = 0;

%Generate impulse responses for filtering
run('generateFIR.m')
impres1 = b1;
impres2 = b2;
%{
fileImp1 = fopen('coef_n_01.txt','w');
fileImp2 = fopen('coef_n_02.txt','w');
fprintf(fileImp1,'%d\n',round(impres1*intScale,0));
fprintf(fileImp2,'%d\n',round(impres2*intScale,0));
fclose(fileImp1);
fclose(fileImp2);
%}

%% Hardware Simulation Loops
fprintf('\nStarting Simulation Loops...\n');

%Canonical FIR
regIn = zeros(1,order);
regOut = regIn;
w = regOut;
FIR = zeros(1,length(refData)+order-1);

for i = 1:length(FIR)
    %First we update the incoming data to be the next sample
    if i <= length(refData)
        regIn(1) = refData(i);
    else
        regIn(1) = 0;
    end
    
    %Then we clock the system
    regOut = regIn;

    %When the registers update the system calculates the new values
    out = 0;
    for ii = 1:order
        %In the canonical FIR structure, each w node is simply equal to the corresponding register output
        w(ii) = regOut(ii);
        
        %And then we sum up all w nodes multiplied with their corresponding filter coefficient
        if i < change*4
            out = out + w(ii)*impres1(ii);
        else
            out = out + w(ii)*impres2(ii);
        end
    end
    FIR(i) = out;
    
    %When the system is stable the register inputs are ready for the next cycle
    regIn = [regIn(1), w(1:order-1)];
end
FIRcn = FIR;
fprintf('Canonical FIR Complete at: %s\n', datetime("now"));

%Transposed FIR
regIn = zeros(1,order);
regOut = regIn;
w = regOut;
FIR = zeros(1,length(refData)+order-1);

for i = 1:length(FIR)
    %First we update the incoming data to be the next sample
    if i <= length(refData)
        regIn(1) = refData(i);
    else
        regIn(1) = 0;
    end
    
    %Then we clock the system
    regOut = regIn;

    %When the registers update the system calculates the new values
    for ii = 1:order
        %In the transposed FIR structure, each w node is equal to the sum of the reg before it and the inverse-corresponding coefficient multiplied with regOut(1)
        if i < change*4
            w(ii) = regOut(1)*impres1(order - ii + 1);
        else
            w(ii) = regOut(1)*impres2(order - ii + 1);
        end

        if ii > 1
            w(ii) = w(ii) + regOut(ii);
        end
    end
    FIR(i) = w(end);
    
    %When the system is stable the register inputs are ready for the next cycle
    regIn = [regIn(1), w(1:order-1)];
end
FIRtn = FIR;
fprintf('Transposed FIR Complete at: %s\n', datetime("now"));



%% Warped FIR Setup
warped = 1;
lambda = 0.75;

run('generateFIR.m')

impres1 = b1;
impres2 = b2;
%{
fileImp1 = fopen('coef_w_01.txt','w');
fileImp2 = fopen('coef_w_02.txt','w');
fprintf(fileImp1,'%d\n',round(impres1*intScale,0));
fprintf(fileImp2,'%d\n',round(impres2*intScale,0));
fclose(fileImp1);
fclose(fileImp2);
%}

%% Hardware Simulation Loops

%Canonical WFIR
regIn = zeros(1,order+1);
regOut = regIn;
w = zeros(1,order);
FIR = zeros(1,length(refData)+order-1);

for i = 1:length(FIR)
    %First we update the incoming data to be the next sample
    if i <= length(refData)
        regIn(1) = refData(i);
    else
        regIn(1) = 0;
    end
    
    %Then we clock the system
    regOut = regIn;

    %When the registers update the system calculates the new values
    w(1) = regOut(1);
    out = w(1)*impres1(1);
    for ii = 2:order
        %In the canonical WFIR structure, each w node is equal to:
        w(ii) = regOut(ii)+(regOut(ii+1)-w(ii-1))*lambda;
        
        %And then we sum up all w nodes multiplied with their corresponding filter coefficient
        if i < change*4
            out = out + w(ii)*impres1(ii);
        else
            out = out + w(ii)*impres2(ii);
        end
    end
    FIR(i) = out;
    
    %When the system is stable the register inputs are ready for the next cycle
    regIn = [regIn(1), w(1:order)];
end
FIRcw = FIR;
fprintf('Canonical WFIR Complete at: %s\n', datetime("now"));


%Transposed WFIR
regIn = zeros(1,order+1);
regOut = regIn;
w = zeros(1,order);
FIR = zeros(1,length(refData)+order-1);

for i = 1:length(FIR)
    %First we update the incoming data to be the next sample
    if i <= length(refData)
        regIn(1) = refData(i);
    else
        regIn(1) = 0;
    end
    
    %Then we clock the system
    regOut = regIn;

    %When the registers update the system calculates the new values
    for ii = 1:order
        %In the transposed WFIR structure, each w node is equal to the sum of the reg before it and the inverse-corresponding coefficient multiplied with regOut(1)
        if i < change*4
            w(ii) = regOut(1)*impres1(order - ii + 1);
        else
            w(ii) = regOut(1)*impres2(order - ii + 1);
        end

        if ii > 1
            w(ii) = w(ii) + regOut(ii)+(regOut(ii+1)-w(ii-1))*lambda;
        end
    end
    FIR(i) = w(end);
    
    %When the system is stable the register inputs are ready for the next cycle
    regIn = [regIn(1), w];
end
FIRtw = FIR;
fprintf('Transposed WFIR Complete at: %s\n', datetime("now"));



%
%% Loading of hardware results from files into Matlab
fileFIRcnH = fopen(strcat('Test Data/test_data_cnH_', file(1:4),'.txt'),'r');
fileFIRcwH = fopen(strcat('Test Data/test_data_cwH_', file(1:4),'.txt'),'r');
fileFIRtnH = fopen(strcat('Test Data/test_data_tnH_', file(1:4),'.txt'),'r');
fileFIRtwH = fopen(strcat('Test Data/test_data_twH_', file(1:4),'.txt'),'r');
FIRcnH = transpose(fscanf(fileFIRcnH, '%d')/(intScale^2));
FIRcwH = transpose(fscanf(fileFIRcwH, '%d')/(intScale^2));
FIRtnH = transpose(fscanf(fileFIRtnH, '%d')/(intScale^2));
FIRtwH = transpose(fscanf(fileFIRtwH, '%d')/(intScale^2));
fclose(fileFIRcnH);
fclose(fileFIRcwH);
fclose(fileFIRtnH);
fclose(fileFIRtwH);
%}

%
%% Do PEAQ on all tests based on clean simulation
refFile = 'refData.wav';
testFile = 'testData.wav';
ref = [FIRcn; FIRtn; FIRcw; FIRtw];
test = [FIRcnH; FIRtnH; FIRcwH; FIRtwH];
check = ['FIRcn'; 'FIRtn'; 'FIRcw'; 'FIRtw'];

ODG = ["" "" "" ""];
for j = 1:4
    fprintf('Running PEAQ for %s at: %s\n', check(j,:), datetime("now"));
    audiowrite(refFile, ref(j,:), Fs);
    audiowrite(testFile, test(j,:), Fs);
    run("peaqScript.m");

    if isnan(objectiveDifferenceGrade)
        ODG(j) = 'NaN';
    else
        ODG(j) = num2str(objectiveDifferenceGrade);
    end

    delete(refFile);
    delete(testFile);
end
%}

%
%% Plots
fprintf('\nStarting First Plot...\n');
file(1) = upper(file(1));



figure(1)
subplot(4,1,1)
yLim = ceil(max(refDataFull)*4)/4;
hold on;
plot(refDataFull)
plot([fileOffset, fileOffset],[yLim, -yLim], '--', 'Color', [0.6 0 0])
plot([fileOffset+fileLength-1, fileOffset+fileLength-1],[yLim, -yLim], '--', 'Color', [0.6 0 0])
hold off;
ylim([-yLim, yLim])
xlabel('Sample')
ylabel('Amplitude')
title(strcat('Cutouf of Full Audio File -', " ", file))

subplot(4,1,[2 4])
color1 = [0.6350 0.0780 0.1840];
color2 = [0.4660 0.6740 0.1880];
color3 = [0.0000 0.4470 0.7410];
color4 = [0.8500 0.3250 0.0980];
hold on;
plot([4*change, 4*change],[9, -1], '--', 'Color', [0 0 0])
plot(1:(fileLength), refData+8, 'Color', [0.3010 0.7450 0.9330])
plot(1:(fileLength+order-1), FIRcn+7, 'Color', color1)
plot(1:(fileLength+order-1), FIRtn+6, 'Color', color2)
plot(1:(fileLength+order-1), FIRcw+5, 'Color', color3)
if strcmp(file, 'Piano')
    plot(1:(fileLength+order-1), FIRcwH+1, 'Color', color3)
    plot(1:(fileLength+order-1), FIRtw+4, 'Color', color4)
    plot(1:(fileLength+order-1), FIRcnH+3, 'Color', color1)
    plot(1:(fileLength+order-1), FIRtnH+2, 'Color', color2)
else
    plot(1:(fileLength+order-1), FIRtw+4, 'Color', color4)
    plot(1:(fileLength+order-1), FIRcnH+3, 'Color', color1)
    plot(1:(fileLength+order-1), FIRtnH+2, 'Color', color2)
    plot(1:(fileLength+order-1), FIRcwH+1, 'Color', color3)
    
end
plot(1:(fileLength+order-1), FIRtwH, 'Color', color4)

hold off;
xlim([0 fileLength+256])
ylim([-1, 9])
yticks(0:8)
yticklabels(["Hardware Transposed WFIR", "Hardware Canonical WFIR", "Hardware Transposed FIR", "Hardware Canonical FIR", "Simulated Transposed WFIR", "Simulated Canonical WFIR", "Simulated Transposed FIR", "Simulated Canonical FIR", "Input Signal"])
xlabel('Local Sample')
ylabel('Test')
title(strcat('Comparison Between Filtered Signals With Coefficient Change -', " ", file))
yyaxis right
ylim([-1, 9])
yticks(0:8)
yticklabels([ODG(4), ODG(3), ODG(2), ODG(1), "", "", "", "", ""])
ylabel('Hardware PEAQ ODG Scores')
set(gca,'YColor',[0 0 0]);

set(gcf,'position',[10,10,1800,900])
saveas(gcf, strcat("Simulation vs Hardware Filtering Comparison (", file, ").png"))



figure(2)
L = fileLength + order - 1;
freq = (-L/2:L/2-1)*(Fs/L);
names = ['Canonical FIR  '; 'Transposed FIR '; 'Canonical WFIR '; 'Transposed WFIR'];
for i = 1:4
    subplot(4,4,[1 3]+(i-1)*4)
    hold on;
    plot(freq, mag2db(abs(fftshift(fft(test(i,:)))))+50)
    plot(freq, mag2db(abs(fftshift(fft(ref(i,:))))))
    hold off;
    legend('Hardware Filtered [+50dB]', 'Simulated')
    xlim([0 Fs/2])
    xticks(0:4000:Fs/2)
    xticklabels(string(0:4:Fs/2000))
    ylim([-100, 150])
    title(['FFT comparison of Simulated and Hardware Filtered Signal for ' names(i,:) ' - ' file])
    xlabel('Frequency [kHz]')
    ylabel('Magnitude [dB]')
    subplot(4,4,i*4)
    hold on;
    plot(freq, mag2db(abs(fftshift(fft(test(i,:))))))
    plot(freq, mag2db(abs(fftshift(fft(ref(i,:))))))
    hold off;
    legend('Hardware Filtered', 'Simulated')
    xlim([0 Fs/2])
    xticks(0:4000:Fs/2)
    xticklabels(string(0:4:Fs/2000))
    ylim([-100, 150])
    title('Without Offset')
    xlabel('Frequency [kHz]')
    ylabel('Magnitude [dB]')
end
set(gcf,'position',[10,10,1200,900])
saveas(gcf, strcat("Simulation vs Hardware Filtering FFTs (", file, ").png"))



figure(3)
subplot(3,4,[2 3])
spectrogram(refData, 2048, 1024, 2048, Fs, 'yaxis')
title('Input Signal')
subplot(3,4,[5 6])
spectrogram(FIRcn, 2048, 1024, 2048, Fs, 'yaxis')
title('Canonical FIR')
subplot(3,4,[7 8])
spectrogram(FIRtn, 2048, 1024, 2048, Fs, 'yaxis')
title('Transposed FIR')
subplot(3,4,[9 10])
spectrogram(FIRcw, 2048, 1024, 2048, Fs, 'yaxis')
title('Canonical WFIR')
subplot(3,4,[11 12])
spectrogram(FIRtw, 2048, 1024, 2048, Fs, 'yaxis')
title('Transposed WFIR')

set(gcf,'position',[10,10,1200,1200])
saveas(gcf, strcat("Simulation vs Hardware Filtering Spectrograms (", file, ").png"))



figure(4)
L = fileLength + order - 1;
freq = (-L/2:L/2-1)*(Fs/L);
hold on;
plot(freq, mag2db(abs(fftshift(fft(test(2,:))))),'LineWidth',6)
plot(freq, mag2db(abs(fftshift(fft(ref(2,:))))),'LineWidth',4)
hold off;
xlim([0 Fs/2])
xticks(0:4000:Fs/2)
xticklabels(string(0:4:Fs/2000))
ylim([-150, 100])
title('Comparison Between Simulation and Hardware Filtered Signals','FontSize',36)
xlabel('Frequency [kHz]','FontSize',32)
ylabel('Magnitude [dB]','FontSize',32)
grid on;
f = gca;
f.LineWidth = 4;
f.GridLineWidth = 2;
f.FontSize = 32;

set(gcf,'position',[10,10,1800,800])
saveas(gcf, "Simulation vs Hardware Filtering Comparison Subgraph.png")

fprintf('Plots Complete at: %s\n', datetime("now"));

%}
