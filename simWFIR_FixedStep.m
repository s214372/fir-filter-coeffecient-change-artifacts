%Filtering hardware simulation for canonical WFIR structures with coefficient change manager

close all force
clear

%% Setup
order = 256;
scale1 = 16;
scale2 = 56;
change = int32(0x52);
warped = 1;
lambda = 0.75;

%Create tone complex signal
Freqs = [5000 12000 19000];
Fs = 48000;
Ts = 2;

TimeVector = 1/Fs:1/Fs:Ts;
Signal = sin(2*pi*Freqs(1)*TimeVector);
for i = 2:length(Freqs)
    Signal = Signal + sin(2*pi*Freqs(i)*TimeVector);
end
refData = Signal;

if length(refData) > 0x800
    change = change*int32(0x80);
end

run('generateFIR.m')

impres1 = b1;
impres2 = b2;
diff = impres2 - impres1;


%% Define Interpolation Modes
len = 2^7;

interp1 = [zeros(1,floor(len/2)) ones(1,ceil(len/2))];

linear = 1/len:1/len:1;

interp2 = linear;
interp3 = sin((pi/2)*linear);
interp4 = linear.^4;
interp5 = 1 + log(linear)/log(len*2);
interp6 = 1./(1+5.^(10*(-linear+0.5)));


interp = [interp1; interp2; interp3; interp4; interp5; interp6];

runs = length(interp(:,1));


%% Hardware Simulation Loops
FIR = zeros(runs,length(refData)+order-1);
for j = 1:runs
    %Canonical WFIR
    regIn = zeros(1,order+1);
    regOut = regIn;
    w = zeros(1,order);
    
    impres = impres1;
    c = 1;
    
    for i = 1:length(FIR(j,:))
        %First we update the incoming data to be the next sample
        if i <= length(refData)
            regIn(1) = refData(i);
        else
            regIn(1) = 0;
        end
        
        %Then we clock the system
        regOut = regIn;
    
        %When the registers update the system calculates the new values
        w(1) = regOut(1);
        out = w(1)*impres1(1);
        for ii = 2:order
            %In the canonical WFIR structure, each w node is equal to:
            w(ii) = regOut(ii)+(regOut(ii+1)-w(ii-1))*lambda;
            
            %If we are at the sample for a coefficient change, we transform the coefficient array a little every sample
            if i == change*4 - floor(len/2) + c
                impres = impres1+diff*interp(j,c);
    
                c = c + 1;
                if c > len
                    c = 1;
                end
            end
    
            %And then we sum up all w nodes multiplied with their corresponding filter coefficient
            out = out + w(ii)*impres(ii);
        end
        FIR(j,i) = out;
        
        %When the system is stable the register inputs are ready for the next cycle
        regIn = [regIn(1), w(1:order)];
    end
end

val = FIR(2,:);
save(strcat('Test Data/linInterp', num2str(len),'.mat'),"val")



%% Plots

figure(1)

labels = ["No Interpolation", "Linear Interpolation", "Sinusoid Interpolation", "Exponential Interpolation", "Logarithmic Interpolation", "Sigmoid Interpolation"];
for j = 1:runs
    subplot(runs,5,j*5-4)
    plot(0:len, [0 interp(j,:)],'.-')
    xlim([0 len])
    xticks(0:len/4:len)
    ylim([0 1])
    yticks(0:0.25:1)
    grid on;
    xlabel("Interpolation Array Index")
    ylabel("Value at Index")
    title(labels(j))

    subplot(runs,5,[j*5-3 j*5-1])
    hold on;
    data = FIR(j,order:end-order);
    L = length(data);
    FFT = fft(data);
    FFT = fftshift(FFT);
    freq = (-L/2:L/2-1)*(Fs/L);
    mag = mag2db((abs(FFT).^2)/L);
    plot(freq(ceil(length(freq)/2)+1:end),mag(ceil(length(freq)/2)+1:end))
    data = FIR(j,order:4*change-len);
    L = length(data);
    FFT = fft(data);
    FFT = fftshift(FFT);
    freq = (-L/2:L/2-1)*(Fs/L);
    mag = mag2db((abs(FFT).^2)/L);
    plot(freq(ceil(length(freq)/2)+1:end),mag(ceil(length(freq)/2)+1:end))
    replot1 = freq(ceil(length(freq)/2)+1:end);
    replot2 = mag(ceil(length(freq)/2)+1:end);
    data = FIR(j,4*change+order+len:end-order);
    L = length(data);
    FFT = fft(data);
    FFT = fftshift(FFT);
    freq = (-L/2:L/2-1)*(Fs/L);
    mag = mag2db((abs(FFT).^2)/L);
    plot(freq(ceil(length(freq)/2)+1:end),mag(ceil(length(freq)/2)+1:end), "Color", [0 0.7 0.2])
    plot(replot1(floor(length(replot1)*0.4):floor(length(replot1)*0.6)),replot2(floor(length(replot2)*0.4):floor(length(replot2)*0.6)), "Color", [0.8500 0.3250 0.0980])
    hold off;
    ylim([-200 200])
    xlim([0 Fs/2])
    xticks(0:4000:Fs/2)
    xticklabels(string(0:4:Fs/2000))
    grid on;
    legend('Signal With Change','Before Change','After Change')
    title(strcat("FFT of Filtered Signal - ", labels(j)))
    xlabel('Frequency [kHz]')
    ylabel('Magnitude [dB]')

    subplot(runs,5,j*5)
    spectrogram(FIR(j,4*change-len-order*2:4*change+order*2+len), 32, 8, 32, Fs, 'yaxis')
    title('Spectrogram')
end

set(gcf,'position',[10,10,1800,2000])
saveas(gcf, strcat("Fixed WFIR Coefficient Change FFTs - ", num2str(len),"-Point Interpolation.png"))



figure(2)

load 'Test Data\linInterp8.mat'
lin8 = val;
load 'Test Data\linInterp32.mat'
lin32 = val;
load 'Test Data\linInterp128.mat'
lin128 = val;
load 'Test Data\linInterp512.mat'
lin512 = val;

ref = [FIR(1,:);lin32;lin128];
labels = ["No Interpolation", "32-Point Interpolation", "128-Point Interpolation"];

for j = 1:length(ref(:,1))
    subplot(4,length(ref(:,1))*4-1,[j*4-3+1*length(ref(:,1))*4-1 j*4-1+2*length(ref(:,1))*4-2])
    spectrogram(ref(j,4*change-order*2:4*change+order*2), 32, 8, 32, Fs, 'yaxis')
    f = gca;
    f.FontSize = 20;
    xlabel('Sample','FontSize',20)
    ylabel('Frequency [kHz]','FontSize',20)
    c = colorbar;
    c.Label.String = 'Power/Frequency [dB/Hz]';
    c.Label.FontSize = 20;
    title(labels(j),'FontSize',24)
end

set(gcf,'position',[10,10,1800,800])
saveas(gcf, 'Fixed WFIR Coefficient Change Spectrograms Subgraph.png')



figure(3)
hold on;
data = FIR(3,order:end-order);
L = length(data);
FFT = fft(data);
FFT = fftshift(FFT);
freq = (-L/2:L/2-1)*(Fs/L);
mag = mag2db((abs(FFT).^2)/L);
plot(freq(ceil(length(freq)/2)+1:end),mag(ceil(length(freq)/2)+1:end),'LineWidth',6)

data = FIR(3,order:4*change-len);
L = length(data);
FFT = fft(data);
FFT = fftshift(FFT);
freq = (-L/2:L/2-1)*(Fs/L);
mag = mag2db((abs(FFT).^2)/L);
plot(freq(ceil(length(freq)/2)+1:end),mag(ceil(length(freq)/2)+1:end),'LineWidth',4)

replot1 = freq(ceil(length(freq)/2)+1:end);
replot2 = mag(ceil(length(freq)/2)+1:end);

data = FIR(3,4*change+order+len:end-order);
L = length(data);
FFT = fft(data);
FFT = fftshift(FFT);
freq = (-L/2:L/2-1)*(Fs/L);
mag = mag2db((abs(FFT).^2)/L);
plot(freq(ceil(length(freq)/2)+1:end),mag(ceil(length(freq)/2)+1:end),'LineWidth',4, "Color", [0 0.7 0.2])

plot(replot1(floor(length(replot1)*0.4):floor(length(replot1)*0.6)),replot2(floor(length(replot2)*0.4):floor(length(replot2)*0.6)),'LineWidth',4, "Color", [0.8500 0.3250 0.0980])

hold off;
xlim([0 Fs/2])
xticks(0:4000:Fs/2)
xticklabels(string(0:4:Fs/2000))
ylim([-200 150])
grid on;
f = gca;
f.LineWidth = 4;
f.GridLineWidth = 2;
f.FontSize = 32;
title("FFT of Filtered Signal - Sinusoid Interpolation",'FontSize',36)
xlabel('Frequency [kHz]','FontSize',32)
ylabel('Magnitude [dB]','FontSize',32)

set(gcf,'position',[10,10,1800,600])
saveas(gcf, 'Fixed WFIR Coefficient Change FFTs Subgraph.png')

%}