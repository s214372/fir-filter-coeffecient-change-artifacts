%Tests of GstPEAQ implementation

%clear

%% Piano
refFile = 'gstpeaq-develop github\vs\x64\Release\sounds\piano.wav';
testFile = 'testData.wav';

[refData,Fs] = audioread(refFile);

L = length(refData);
FFT = fft(refData);
FFT = fftshift(FFT);
freq1 = (-L/2:L/2-1)*(Fs/L);
mag1 = mag2db((abs(FFT).^2)/L);



%% Get peaq values for file with increasing noise and filtering - Piano
samples = 1000;
step1 = ceil(L/((samples-1)*2));
%{
ODG = 1:samples;
DI = 1:samples;
for j = 1:length(ODG)
    ODG(j) = NaN;
    DI(j) = NaN;
end

fprintf('Start Time: %s\n', datetime("now"));
tic

fprintf('GND Noise Progress: [');
for k = 1:floor(samples/8)
    fprintf('-');
end
fprintf(']');

for j = 1:length(ODG)

    %Add noise with every sample
    if j > 1
        for k = 0:floor(L/(samples*8))
            refData(floor((j-1)*L/samples)+k) = 0;
        end
    end

    audiowrite(testFile,refData,Fs);
    run("peaqScript.m");
    ODG(j) = objectiveDifferenceGrade;
    DI(j) = distortionIndex;

    if mod(j, 8) == 0
        for k = 0:floor(samples/8)
            fprintf('\b');
        end
        for k = 1:floor(j/8)
            fprintf('#');
        end
        for k = 1:floor((samples-j)/8)
            fprintf('-');
        end
        fprintf(']');
    end
end
fprintf('\n');
GN1ODG = ODG;
GN1DI = ODG;



fprintf('VDD Noise Progress: [');
[refData,Fs] = audioread(refFile);
for k = 1:floor(samples/8)
    fprintf('-');
end
fprintf(']');

for j = 1:length(ODG)

    %Add noise with every sample
    if j > 1
        for k = 0:floor(L/(samples*8))
            refData(floor((j-1)*L/samples)+k) = 1;
        end
    end

    audiowrite(testFile,refData,Fs);
    run("peaqScript.m");
    ODG(j) = objectiveDifferenceGrade;
    DI(j) = distortionIndex;

    if mod(j, 8) == 0
        for k = 0:floor(samples/8)
            fprintf('\b');
        end
        for k = 1:floor(j/8)
            fprintf('#');
        end
        for k = 1:floor((samples-j)/8)
            fprintf('-');
        end
        fprintf(']');
    end
end
fprintf('\n');
VN1ODG = ODG;
VN1DI = DI;



fprintf('Random Noise Progress: [');
[refData,Fs] = audioread(refFile);
for k = 1:floor(samples/8)
    fprintf('-');
end
fprintf(']');
rng(round(second(datetime("now"))));

for j = 1:length(ODG)

    %Add noise with every sample
    if j > 1
        for k = 0:floor(L/(samples*8))
            refData(floor((j-1)*L/samples)+k) = rand();
        end
    end

    audiowrite(testFile,refData,Fs);
    run("peaqScript.m");
    ODG(j) = objectiveDifferenceGrade;
    DI(j) = distortionIndex;

    if mod(j, 8) == 0
        for k = 0:floor(samples/8)
            fprintf('\b');
        end
        for k = 1:floor(j/8)
            fprintf('#');
        end
        for k = 1:floor((samples-j)/8)
            fprintf('-');
        end
        fprintf(']');
    end
end
fprintf('\n');
RN1ODG = ODG;
RN1DI = DI;



fprintf('Lowpass Filter Progress: [');
[refData,Fs] = audioread(refFile);
testData = refData;
for k = 1:floor(samples/8)
    fprintf('-');
end
fprintf(']');

for j = 1:length(ODG)

    if j > 1
        f_c = (j-1)*step1*(Fs/L);
        if f_c > Fs/2
            f_c = Fs/2;
        end
        testData = lowpass(refData, Fs/2 - f_c + 1, Fs);
    end

    audiowrite(testFile,testData,Fs);
    run("peaqScript.m");
    ODG(j) = objectiveDifferenceGrade;
    DI(j) = distortionIndex;

    if mod(j, 8) == 0
        for k = 0:floor(samples/8)
            fprintf('\b');
        end
        for k = 1:floor(j/8)
            fprintf('#');
        end
        for k = 1:floor((samples-j)/8)
            fprintf('-');
        end
        fprintf(']');
    end
end
fprintf('\n');
LP1ODG = ODG;
LP1DI = DI;



fprintf('Highpass Filter Progress: [');
[refData,Fs] = audioread(refFile);
testData = refData;
for k = 1:floor(samples/8)
    fprintf('-');
end
fprintf(']');

for j = 1:length(ODG)

    if j > 1
        f_c = (j-1)*step1*(Fs/L);
        if f_c > Fs/2
            f_c = Fs/2;
        end
        testData = highpass(refData, f_c - 2, Fs);
    end

    if (sum(abs(testData)) > 0)
        audiowrite(testFile,testData,Fs);
        run("peaqScript.m");
        ODG(j) = objectiveDifferenceGrade;
        DI(j) = distortionIndex;
    else
        ODG(j) = NaN;
        DI(j) = NaN;
    end

    if mod(j, 8) == 0
        if(sum(isnan(ODG(j-7:j))) == 8)
            for k = 0:floor(samples/8)
                fprintf('\b');
            end
            for k = 1:floor(j/8)
                fprintf('#');
            end
            for k = 1:floor((samples-j)/8)
                fprintf(' ');
            end
            fprintf(']');
            
            for j = j:length(ODG)
                ODG(j) = NaN;
                DI(j) = NaN;
            end

            break
        end

        for k = 0:floor(samples/8)
            fprintf('\b');
        end
        for k = 1:floor(j/8)
            fprintf('#');
        end
        for k = 1:floor((samples-j)/8)
            fprintf('-');
        end
        fprintf(']');
    end
end
fprintf('\n');
HP1ODG = ODG;
HP1DI = DI;



fprintf('Lowpass Cutoff Progress: [');
[refData,Fs] = audioread(refFile);
FFT = fftshift(fft(refData));
c = 1;

for k = 1:floor(samples/8)
    fprintf('-');
end
fprintf(']');

for j = 1:length(ODG)

    %Filter with highpass cutoff in fft array
    if j > 1
        for c = c:(c+step1)
            FFT(c) = 0;
            FFT(L-(c-1)) = 0;
        end
        testData = real(ifft(fftshift(FFT), L));
    else
        testData = refData;
    end
    
    audiowrite(testFile,testData,Fs);
    run("peaqScript.m");
    ODG(j) = objectiveDifferenceGrade;
    DI(j) = distortionIndex;

    if mod(j, 8) == 0
        for k = 0:floor(samples/8)
            fprintf('\b');
        end
        for k = 1:floor(j/8)
            fprintf('#');
        end
        for k = 1:floor((samples-j)/8)
            fprintf('-');
        end
        fprintf(']');
    end
end
fprintf('\n');
LC1ODG = ODG;
LC1DI = DI;



fprintf('Highpass Cutoff Progress: [');
[refData,Fs] = audioread(refFile);
FFT = fft(refData);
c = 1;

for k = 1:floor(samples/8)
    fprintf('-');
end
fprintf(']');

for j = 1:length(ODG)

    %Filter with highpass cutoff in fft array
    if j > 1
        for c = c:(c+step1)
            FFT(c) = 0;
            FFT(L-(c-1)) = 0;
        end
        testData = real(ifft(FFT, L));
    else
        testData = refData;
    end
    
    if (sum(abs(testData)) > 0)
        audiowrite(testFile,testData,Fs);
        run("peaqScript.m");
        ODG(j) = objectiveDifferenceGrade;
        DI(j) = distortionIndex;
    else
        ODG(j) = NaN;
        DI(j) = NaN;
    end

    if mod(j, 8) == 0
        if(sum(isnan(ODG(j-7:j))) == 8)
            for k = 0:floor(samples/8)
                fprintf('\b');
            end
            for k = 1:floor(j/8)
                fprintf('#');
            end
            for k = 1:floor((samples-j)/8)
                fprintf(' ');
            end
            fprintf(']');
            
            for j = j:length(ODG)
                ODG(j) = NaN;
                DI(j) = NaN;
            end

            break
        end

        for k = 0:floor(samples/8)
            fprintf('\b');
        end
        for k = 1:floor(j/8)
            fprintf('#');
        end
        for k = 1:floor((samples-j)/8)
            fprintf('-');
        end
        fprintf(']');
    end
end
fprintf('\n');
HC1ODG = ODG;
HC1DI = DI;

toc
delete(testFile);
%}
step1 = step1*(Fs/L);


%% Rock
refFile = 'gstpeaq-develop github\vs\x64\Release\sounds\rock.wav';
testFile = 'testData.wav';

[refData,Fs] = audioread(refFile);

L = length(refData);
FFT = fft(refData);
FFT = fftshift(FFT);
freq2 = (-L/2:L/2-1)*(Fs/L);
mag2 = mag2db((abs(FFT).^2)/L);


%% Get peaq values for file with increasing noise and filtering - Rock
step2 = ceil(L/((samples-1)*2));
%{
fprintf('Start Time: %s\n', datetime("now"));
tic

fprintf('GND Noise Progress: [');
for k = 1:floor(samples/8)
    fprintf('-');
end
fprintf(']');

for j = 1:length(ODG)

    %Add noise with every sample
    if j > 1
        for k = 0:floor(L/(samples*8))
            refData(floor((j-1)*L/samples)+k) = 0;
        end
    end

    audiowrite(testFile,refData,Fs);
    run("peaqScript.m");
    ODG(j) = objectiveDifferenceGrade;
    DI(j) = distortionIndex;

    if mod(j, 8) == 0
        for k = 0:floor(samples/8)
            fprintf('\b');
        end
        for k = 1:floor(j/8)
            fprintf('#');
        end
        for k = 1:floor((samples-j)/8)
            fprintf('-');
        end
        fprintf(']');
    end
end
fprintf('\n');
GN2 = ODG;



fprintf('VDD Noise Progress: [');
[refData,Fs] = audioread(refFile);
for k = 1:floor(samples/8)
    fprintf('-');
end
fprintf(']');

for j = 1:length(ODG)

    %Add noise with every sample
    if j > 1
        for k = 0:floor(L/(samples*8))
            refData(floor((j-1)*L/samples)+k) = 1;
        end
    end

    audiowrite(testFile,refData,Fs);
    run("peaqScript.m");
    ODG(j) = objectiveDifferenceGrade;
    DI(j) = distortionIndex;

    if mod(j, 8) == 0
        for k = 0:floor(samples/8)
            fprintf('\b');
        end
        for k = 1:floor(j/8)
            fprintf('#');
        end
        for k = 1:floor((samples-j)/8)
            fprintf('-');
        end
        fprintf(']');
    end
end
fprintf('\n');
VN2 = ODG;



fprintf('Random Noise Progress: [');
[refData,Fs] = audioread(refFile);
for k = 1:floor(samples/8)
    fprintf('-');
end
fprintf(']');
rng(round(second(datetime("now"))));

for j = 1:length(ODG)

    %Add noise with every sample
    if j > 1
        for k = 0:floor(L/(samples*8))
            refData(floor((j-1)*L/samples)+k) = rand();
        end
    end

    audiowrite(testFile,refData,Fs);
    run("peaqScript.m");
    ODG(j) = objectiveDifferenceGrade;
    DI(j) = distortionIndex;

    if mod(j, 8) == 0
        for k = 0:floor(samples/8)
            fprintf('\b');
        end
        for k = 1:floor(j/8)
            fprintf('#');
        end
        for k = 1:floor((samples-j)/8)
            fprintf('-');
        end
        fprintf(']');
    end
end
fprintf('\n');
RN2 = ODG;



fprintf('Lowpass Filter Progress: [');
[refData,Fs] = audioread(refFile);
testData = refData;
for k = 1:floor(samples/8)
    fprintf('-');
end
fprintf(']');

for j = 1:length(ODG)

    if j > 1
        f_c = (j-1)*step2*(Fs/L);
        if f_c > Fs/2
            f_c = Fs/2;
        end
        testData = lowpass(refData, Fs/2 - f_c + 1, Fs);
    end

    audiowrite(testFile,testData,Fs);
    run("peaqScript.m");
    ODG(j) = objectiveDifferenceGrade;
    DI(j) = distortionIndex;

    if mod(j, 8) == 0
        for k = 0:floor(samples/8)
            fprintf('\b');
        end
        for k = 1:floor(j/8)
            fprintf('#');
        end
        for k = 1:floor((samples-j)/8)
            fprintf('-');
        end
        fprintf(']');
    end
end
fprintf('\n');
LP2 = ODG;



fprintf('Highpass Filter Progress: [');
[refData,Fs] = audioread(refFile);
testData = refData;
for k = 1:floor(samples/8)
    fprintf('-');
end
fprintf(']');

for j = 1:length(ODG)

    if j > 1
        f_c = (j-1)*step2*(Fs/L);
        if f_c > Fs/2
            f_c = Fs/2;
        end
        testData = highpass(refData, f_c - 2, Fs);
    end

    if (sum(abs(testData)) > 0)
        audiowrite(testFile,testData,Fs);
        run("peaqScript.m");
        ODG(j) = objectiveDifferenceGrade;
        DI(j) = distortionIndex;
    else
        ODG(j) = NaN;
        DI(j) = NaN;
    end

    if mod(j, 8) == 0
        if(sum(isnan(ODG(j-7:j))) == 8)
            for k = 0:floor(samples/8)
                fprintf('\b');
            end
            for k = 1:floor(j/8)
                fprintf('#');
            end
            for k = 1:floor((samples-j)/8)
                fprintf(' ');
            end
            fprintf(']');
            
            for j = j:length(ODG)
                ODG(j) = NaN;
                DI(j) = NaN;
            end

            break
        end

        for k = 0:floor(samples/8)
            fprintf('\b');
        end
        for k = 1:floor(j/8)
            fprintf('#');
        end
        for k = 1:floor((samples-j)/8)
            fprintf('-');
        end
        fprintf(']');
    end
end
fprintf('\n');
HP2 = ODG;



fprintf('Lowpass Cutoff Progress: [');
[refData,Fs] = audioread(refFile);
FFT = fftshift(fft(refData));
c = 1;

for k = 1:floor(samples/8)
    fprintf('-');
end
fprintf(']');

for j = 1:length(ODG)

    %Filter with highpass cutoff in fft array
    if j > 1
        for c = c:(c+step2)
            FFT(c) = 0;
            FFT(L-(c-1)) = 0;
        end
        testData = real(ifft(fftshift(FFT), L));
    else
        testData = refData;
    end
    
    audiowrite(testFile,testData,Fs);
    run("peaqScript.m");
    ODG(j) = objectiveDifferenceGrade;
    DI(j) = distortionIndex;

    if mod(j, 8) == 0
        for k = 0:floor(samples/8)
            fprintf('\b');
        end
        for k = 1:floor(j/8)
            fprintf('#');
        end
        for k = 1:floor((samples-j)/8)
            fprintf('-');
        end
        fprintf(']');
    end
end
fprintf('\n');
LC2 = ODG;



fprintf('Highpass Cutoff Progress: [');
[refData,Fs] = audioread(refFile);
FFT = fft(refData);
c = 1;

for k = 1:floor(samples/8)
    fprintf('-');
end
fprintf(']');

for j = 1:length(ODG)

    %Filter with highpass cutoff in fft array
    if j > 1
        for c = c:(c+step2)
            FFT(c) = 0;
            FFT(L-(c-1)) = 0;
        end
        testData = real(ifft(FFT, L));
    else
        testData = refData;
    end
    
    if (sum(abs(testData)) > 0)
        audiowrite(testFile,testData,Fs);
        run("peaqScript.m");
        ODG(j) = objectiveDifferenceGrade;
        DI(j) = distortionIndex;
    else
        ODG(j) = NaN;
        DI(j) = NaN;
    end

    if mod(j, 8) == 0
        if(sum(isnan(ODG(j-7:j))) == 8)
            for k = 0:floor(samples/8)
                fprintf('\b');
            end
            for k = 1:floor(j/8)
                fprintf('#');
            end
            for k = 1:floor((samples-j)/8)
                fprintf(' ');
            end
            fprintf(']');
            
            for j = j:length(ODG)
                ODG(j) = NaN;
                DI(j) = NaN;
            end

            break
        end

        for k = 0:floor(samples/8)
            fprintf('\b');
        end
        for k = 1:floor(j/8)
            fprintf('#');
        end
        for k = 1:floor((samples-j)/8)
            fprintf('-');
        end
        fprintf(']');
    end
end
fprintf('\n');
HC2 = ODG;

toc
delete(testFile);
%}
step2 = step2*(Fs/L);




%% Orchestra
refFile = 'gstpeaq-develop github\vs\x64\Release\sounds\orchestra.wav';
testFile = 'testData.wav';

[refData,Fs] = audioread(refFile);

L = length(refData);
FFT = fft(refData);
FFT = fftshift(FFT);
freq3 = (-L/2:L/2-1)*(Fs/L);
mag3 = mag2db((abs(FFT).^2)/L);



%% Get peaq values for file with increasing noise and filtering - Orchestra
step3 = ceil(L/((samples-1)*2));
%{
ODG = 1:samples;
DI = 1:samples;
for j = 1:length(ODG)
    ODG(j) = NaN;
    DI(j) = NaN;
end

fprintf('Start Time: %s\n', datetime("now"));
tic

fprintf('GND Noise Progress: [');
for k = 1:floor(samples/8)
    fprintf('-');
end
fprintf(']');

for j = 1:length(ODG)

    %Add noise with every sample
    if j > 1
        for k = 0:floor(L/(samples*8))
            refData(floor((j-1)*L/samples)+k) = 0;
        end
    end

    audiowrite(testFile,refData,Fs);
    run("peaqScript.m");
    ODG(j) = objectiveDifferenceGrade;
    DI(j) = distortionIndex;

    if mod(j, 8) == 0
        for k = 0:floor(samples/8)
            fprintf('\b');
        end
        for k = 1:floor(j/8)
            fprintf('#');
        end
        for k = 1:floor((samples-j)/8)
            fprintf('-');
        end
        fprintf(']');
    end
end
fprintf('\n');
GN3 = ODG;



fprintf('VDD Noise Progress: [');
[refData,Fs] = audioread(refFile);
for k = 1:floor(samples/8)
    fprintf('-');
end
fprintf(']');

for j = 1:length(ODG)

    %Add noise with every sample
    if j > 1
        for k = 0:floor(L/(samples*8))
            refData(floor((j-1)*L/samples)+k) = 1;
        end
    end

    audiowrite(testFile,refData,Fs);
    run("peaqScript.m");
    ODG(j) = objectiveDifferenceGrade;
    DI(j) = distortionIndex;

    if mod(j, 8) == 0
        for k = 0:floor(samples/8)
            fprintf('\b');
        end
        for k = 1:floor(j/8)
            fprintf('#');
        end
        for k = 1:floor((samples-j)/8)
            fprintf('-');
        end
        fprintf(']');
    end
end
fprintf('\n');
VN3 = ODG;



fprintf('Random Noise Progress: [');
[refData,Fs] = audioread(refFile);
for k = 1:floor(samples/8)
    fprintf('-');
end
fprintf(']');
rng(round(second(datetime("now"))));

for j = 1:length(ODG)

    %Add noise with every sample
    if j > 1
        for k = 0:floor(L/(samples*8))
            refData(floor((j-1)*L/samples)+k) = rand();
        end
    end

    audiowrite(testFile,refData,Fs);
    run("peaqScript.m");
    ODG(j) = objectiveDifferenceGrade;
    DI(j) = distortionIndex;

    if mod(j, 8) == 0
        for k = 0:floor(samples/8)
            fprintf('\b');
        end
        for k = 1:floor(j/8)
            fprintf('#');
        end
        for k = 1:floor((samples-j)/8)
            fprintf('-');
        end
        fprintf(']');
    end
end
fprintf('\n');
RN3 = ODG;



fprintf('Lowpass Filter Progress: [');
[refData,Fs] = audioread(refFile);
testData = refData;
for k = 1:floor(samples/8)
    fprintf('-');
end
fprintf(']');

for j = 1:length(ODG)

    if j > 1
        f_c = (j-1)*step3*(Fs/L);
        if f_c > Fs/2
            f_c = Fs/2;
        end
        testData = lowpass(refData, Fs/2 - f_c + 1, Fs);
    end

    audiowrite(testFile,testData,Fs);
    run("peaqScript.m");
    ODG(j) = objectiveDifferenceGrade;
    DI(j) = distortionIndex;

    if mod(j, 8) == 0
        for k = 0:floor(samples/8)
            fprintf('\b');
        end
        for k = 1:floor(j/8)
            fprintf('#');
        end
        for k = 1:floor((samples-j)/8)
            fprintf('-');
        end
        fprintf(']');
    end
end
fprintf('\n');
LP3 = ODG;



fprintf('Highpass Filter Progress: [');
[refData,Fs] = audioread(refFile);
testData = refData;
for k = 1:floor(samples/8)
    fprintf('-');
end
fprintf(']');

for j = 1:length(ODG)

    if j > 1
        f_c = (j-1)*step3*(Fs/L);
        if f_c > Fs/2
            f_c = Fs/2;
        end
        testData = highpass(refData, f_c - 2, Fs);
    end

    if (sum(abs(testData)) > 0)
        audiowrite(testFile,testData,Fs);
        run("peaqScript.m");
        ODG(j) = objectiveDifferenceGrade;
        DI(j) = distortionIndex;
    else
        ODG(j) = NaN;
        DI(j) = NaN;
    end

    if mod(j, 8) == 0
        if(sum(isnan(ODG(j-7:j))) == 8)
            for k = 0:floor(samples/8)
                fprintf('\b');
            end
            for k = 1:floor(j/8)
                fprintf('#');
            end
            for k = 1:floor((samples-j)/8)
                fprintf(' ');
            end
            fprintf(']');
            
            for j = j:length(ODG)
                ODG(j) = NaN;
                DI(j) = NaN;
            end

            break
        end

        for k = 0:floor(samples/8)
            fprintf('\b');
        end
        for k = 1:floor(j/8)
            fprintf('#');
        end
        for k = 1:floor((samples-j)/8)
            fprintf('-');
        end
        fprintf(']');
    end
end
fprintf('\n');
HP3 = ODG;



fprintf('Lowpass Cutoff Progress: [');
[refData,Fs] = audioread(refFile);
FFT = fftshift(fft(refData));
c = 1;

for k = 1:floor(samples/8)
    fprintf('-');
end
fprintf(']');

for j = 1:length(ODG)

    %Filter with highpass cutoff in fft array
    if j > 1
        for c = c:(c+step3)
            FFT(c) = 0;
            FFT(L-(c-1)) = 0;
        end
        testData = real(ifft(fftshift(FFT), L));
    else
        testData = refData;
    end
    
    audiowrite(testFile,testData,Fs);
    run("peaqScript.m");
    ODG(j) = objectiveDifferenceGrade;
    DI(j) = distortionIndex;

    if mod(j, 8) == 0
        for k = 0:floor(samples/8)
            fprintf('\b');
        end
        for k = 1:floor(j/8)
            fprintf('#');
        end
        for k = 1:floor((samples-j)/8)
            fprintf('-');
        end
        fprintf(']');
    end
end
fprintf('\n');
LC3 = ODG;



fprintf('Highpass Cutoff Progress: [');
[refData,Fs] = audioread(refFile);
FFT = fft(refData);
c = 1;

for k = 1:floor(samples/8)
    fprintf('-');
end
fprintf(']');

for j = 1:length(ODG)

    %Filter with highpass cutoff in fft array
    if j > 1
        for c = c:(c+step3)
            FFT(c) = 0;
            FFT(L-(c-1)) = 0;
        end
        testData = real(ifft(FFT, L));
    else
        testData = refData;
    end
    
    if (sum(abs(testData)) > 0)
        audiowrite(testFile,testData,Fs);
        run("peaqScript.m");
        ODG(j) = objectiveDifferenceGrade;
        DI(j) = distortionIndex;
    else
        ODG(j) = NaN;
        DI(j) = NaN;
    end

    if mod(j, 8) == 0
        if(sum(isnan(ODG(j-7:j))) == 8)
            for k = 0:floor(samples/8)
                fprintf('\b');
            end
            for k = 1:floor(j/8)
                fprintf('#');
            end
            for k = 1:floor((samples-j)/8)
                fprintf(' ');
            end
            fprintf(']');
            
            for j = j:length(ODG)
                ODG(j) = NaN;
                DI(j) = NaN;
            end

            break
        end

        for k = 0:floor(samples/8)
            fprintf('\b');
        end
        for k = 1:floor(j/8)
            fprintf('#');
        end
        for k = 1:floor((samples-j)/8)
            fprintf('-');
        end
        fprintf(']');
    end
end
fprintf('\n');
HC3 = ODG;

toc
delete(testFile);
%}
step3 = step3*(Fs/L);



%% Plot

figure(1);

subplot(3,3,1);
hold on;
plot(freq1, mag1, 'Color', [0 0 0.9]);
ticks1 = ones(1, floor(length(freq1)/2))*47.5;
tmarks1 = 0:5000:25000;
for k = 1:10
    ticks1(round(k*length(ticks1)/10)) = 40;
    tmarks1 = [-k*100*step1 tmarks1];
end
ticks1(1) = 36;
ticks1(round(length(ticks1)/2)) = 36;
ticks1(end) = 36;
tmarks1 = [-25*10^3 tmarks1];
plot(freq1(1:floor(length(freq1)/2)), ticks1, 'Color', [0 0 0]);
hold off;
xlim([-25*10^3 25*10^3])
xticks(tmarks1)
xticklabels(["" "1000" "900" "800" "700" "600" "500" "400" "300" "200" "100" "0" "5k" "10k" "15k" "20k" "25k"])
ylim([-200 50])
grid on;
legend("Signal FFT","Tick Marks Matching Filters' f_c")
title('FFT - Piano Music')
xlabel('Filtering Sample Numbers for Comparison / Frequency [Hz]')
ylabel('Magnitude [dB]')

subplot(3,3,4);
hold on;
plot(1:samples, GN1, "Color", [1 0 0])
plot(1:samples, VN1, "Color", [0 0 1])
plot(1:samples, RN1, "Color", [0 0.8 0])
hold off;
grid on;
legend('GND Noise','VDD Noise','Random Noise')
title('PEAQ ODG Score with Increasing Added Noise in Time Domain - Piano Music')
xlabel('Sample Number')
ylabel('PEAQ Objective Difference Grade')

subplot(3,3,7);
hold on;
plot(1:samples, LP1, "Color", [1 0 0])
plot(1:samples, LC1, "Color", [0.7 0 0])
plot(1:samples, HP1, "Color", [0.2 0.2 1])
plot(1:samples, HC1, "Color", [0 0 0.7])
hold off;
grid on;
legend('LP Filtered','LP FFT-Cutoff','HP Filtered','HP FFT-Cutoff')
title('PEAQ ODG Score with Increasing Filtering - Piano Music')
xlabel('Sample Number')
ylabel('PEAQ Objective Difference Grade')

subplot(3,3,2);
hold on;
plot(freq2, mag2, 'Color', [0 0 0.9]);
ticks2 = ones(1, floor(length(freq2)/2))*47.5;
tmarks2 = 0:5000:25000;
for k = 1:10
    ticks2(round(k*length(ticks2)/10)) = 40;
    tmarks2 = [-k*100*step2 tmarks2];
end
ticks2(1) = 36;
ticks2(round(length(ticks2)/2)) = 36;
ticks2(end) = 36;
tmarks2 = [-25*10^3 tmarks2];
plot(freq2(1:floor(length(freq2)/2)), ticks2, 'Color', [0 0 0]);
hold off;
xlim([-25*10^3 25*10^3])
xticks(tmarks2)
xticklabels(["" "1000" "900" "800" "700" "600" "500" "400" "300" "200" "100" "0" "5k" "10k" "15k" "20k" "25k"])
ylim([-200 50])
grid on;
legend("Signal FFT","Tick Marks Matching Filters' f_c")
title('FFT - Rock Music')
xlabel('Filtering Sample Numbers for Comparison / Frequency [Hz]')
ylabel('Magnitude [dB]')

subplot(3,3,5);
hold on;
plot(1:samples, GN2, "Color", [1 0 0])
plot(1:samples, VN2, "Color", [0 0 1])
plot(1:samples, RN2, "Color", [0 0.8 0])
hold off;
grid on;
legend('GND Noise','VDD Noise','Random Noise')
title('PEAQ ODG Score with Increasing Added Noise in Time Domain - Rock Music')
xlabel('Sample Number')
ylabel('PEAQ Objective Difference Grade')

subplot(3,3,8);
hold on;
plot(1:samples, LP2, "Color", [1 0 0])
plot(1:samples, LC2, "Color", [0.7 0 0])
plot(1:samples, HP2, "Color", [0.2 0.2 1])
plot(1:samples, HC2, "Color", [0 0 0.7])
hold off;
grid on;
legend('LP Filtered','LP FFT-Cutoff','HP Filtered','HP FFT-Cutoff')
title('PEAQ ODG Score with Increasing Filtering - Rock Music')
xlabel('Sample Number')
ylabel('PEAQ Objective Difference Grade')

subplot(3,3,3);
hold on;
plot(freq3, mag3, 'Color', [0 0 0.9]);
ticks3 = ones(1, floor(length(freq3)/2))*47.5;
tmarks3 = 0:5000:25000;
for k = 1:10
    ticks3(round(k*length(ticks3)/10)) = 40;
    tmarks3 = [-k*100*step3 tmarks3];
end
ticks3(1) = 36;
ticks3(round(length(ticks3)/2)) = 36;
ticks3(end) = 36;
tmarks3 = [-25*10^3 tmarks3];
plot(freq3(1:floor(length(freq3)/2)), ticks3, 'Color', [0 0 0]);
hold off;
xlim([-25*10^3 25*10^3])
xticks(tmarks3)
xticklabels(["" "1000" "900" "800" "700" "600" "500" "400" "300" "200" "100" "0" "5k" "10k" "15k" "20k" "25k"])
ylim([-200 50])
grid on;
legend("Signal FFT","Tick Marks Matching Filters' f_c")
title('FFT - Orchestra Music')
xlabel('Filtering Sample Numbers for Comparison / Frequency [Hz]')
ylabel('Magnitude [dB]')

subplot(3,3,6);
hold on;
plot(1:samples, GN3, "Color", [1 0 0])
plot(1:samples, VN3, "Color", [0 0 1])
plot(1:samples, RN3, "Color", [0 0.8 0])
hold off;
grid on;
legend('GND Noise','VDD Noise','Random Noise')
title('PEAQ ODG Score with Increasing Added Noise in Time Domain - Orchestra Music')
xlabel('Sample Number')
ylabel('PEAQ Objective Difference Grade')

subplot(3,3,9);
hold on;
plot(1:samples, LP3, "Color", [1 0 0])
plot(1:samples, LC3, "Color", [0.7 0 0])
plot(1:samples, HP3, "Color", [0.2 0.2 1])
plot(1:samples, HC3, "Color", [0 0 0.7])
hold off;
grid on;
legend('LP Filtered','LP FFT-Cutoff','HP Filtered','HP FFT-Cutoff')
title('PEAQ ODG Score with Increasing Filtering - Orchestra Music')
xlabel('Sample Number')
ylabel('PEAQ Objective Difference Grade')

set(gcf,'position',[10,10,3000,1500])
%annotation('textbox', [0.36, 0.1, 0.1, 0.1], 'String', ["Filters:", "LP f_c = (t-s)*" + round(step1,2) + "Hz", "HP f_c = s*" + round(step1,2) + "Hz"],'FitBoxToText','on','BackgroundColor','w')
%annotation('textbox', [0.64, 0.1, 0.1, 0.1], 'String', ["Filters:", "LP f_c = (t-s)*" + round(step2,2) + "Hz", "HP f_c = s*" + round(step2,2) + "Hz"],'FitBoxToText','on','BackgroundColor','w')
%annotation('textbox', [0.92, 0.1, 0.1, 0.1], 'String', ["Filters:", "LP f_c = (t-s)*" + round(step3,2) + "Hz", "HP f_c = s*" + round(step3,2) + "Hz"],'FitBoxToText','on','BackgroundColor','w')
saveas(gcf, 'PEAQ Noise Figures.png')

%}