#!/usr/bin/env python3
import os

##############################################    
def main():
    
    # Set buffer location in memory
    ID_OFFSET = 0x00001000
    TX_OFFSET = 0x01000000
    RX_OFFSET = 0x02000000
    IRQACK_OFFSET = 0x0103fffc
    
    # Open files
    fd_h2c = os.open("/dev/xdma0_h2c_0", os.O_WRONLY)
    fd_c2h = os.open("/dev/xdma0_c2h_0", os.O_RDONLY)
    fd_event = os.open("/dev/xdma0_events_0", os.O_RDONLY)

    #Check Version ID
    read_val = os.pread(fd_c2h, 16, ID_OFFSET)
    int_val = int.from_bytes(read_val, 'little', signed="False")
    print(f"Version ID read: {str(hex(int_val))}")
    

    #Setup Bits
    canon_en = 1
    trans_en = 0
    warp_en = 1
    fixed_point_en = 0
    mark_1 = 0x40
    mark_2 = 0x00
    mark_3 = 0x00
    mark_4 = 0x00
    
    setup_bits = (mark_4 << 25) | (mark_3 << 18) | (mark_2 << 11) | (mark_1 << 4) | (fixed_point_en << 3) | (warp_en << 2) | (trans_en << 1) | (canon_en << 0)
    print(f"Setup bits: 0x{str(hex(setup_bits))[2:].zfill(8)}") #Print check value
    
    
    #Set up coefficients
    #coef_01 = [-1 for i in range(0,256)]
    if (warp_en == 1):
        with open('coef_w_01.txt') as f:
            coef_01 = [int(l) for l in f]
    else:
        with open('coef_n_01.txt') as f:
            coef_01 = [int(l) for l in f]
    
    
    #coef_02 = [2 for i in range(0,256)]
    if (warp_en == 1):
        with open('coef_w_02.txt') as f:
            coef_02 = [int(l) for l in f]
    else:
        with open('coef_n_02.txt') as f:
            coef_02 = [int(l) for l in f]
            
    print(f"coef_01 Length: {len(coef_01)} (0x{str(hex(len(coef_01)))[2:].zfill(8)}), coef_01[0] = {coef_01[0]} (0x{str(hex(coef_01[0]))[2:].zfill(8)}), coef_01[255] = {coef_01[255]} (0x{str(hex(coef_01[255]))[2:].zfill(8)})") #Print check values
    print(f"coef_02 Length: {len(coef_02)} (0x{str(hex(len(coef_02)))[2:].zfill(8)}), coef_02[0] = {coef_02[0]} (0x{str(hex(coef_02[0]))[2:].zfill(8)}), coef_02[255] = {coef_02[255]} (0x{str(hex(coef_02[255]))[2:].zfill(8)})") #Print check values
    
    
    #Set up ref data
    #ref_data = [0x00000001, 0x00000010, 0x00000100, 0x00001000, 0x00010000, 0x00100000, 0x01000000, 0x10000000, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00]
    with open('ref_data.txt') as f:
        ref_data = [int(l) for l in f]
    
    data_len = len(ref_data)
    print(f"ref_data Length: {data_len} (0x{str(hex(data_len))[2:].zfill(8)}), ref_data[0] = {ref_data[0]} (0x{str(hex(ref_data[0]))[2:].zfill(8)}), ref_data[{data_len-1}] = {ref_data[data_len-1]} (0x{str(hex(ref_data[data_len-1]))[2:].zfill(8)})\n") #Print check values
    
    #Put all the parts into one package
    tx_package = []
    tx_package.append(setup_bits)
    for i in range(0, 256):
        tx_package.append(coef_01[i])
    for i in range(0, 256):
        tx_package.append(coef_02[i])
    tx_package.append(data_len)
    for i in range(0, data_len):
        tx_package.append(ref_data[i])
    for i in range(0, 256):
        tx_package.append(0x00)
    
    
    #Transform into a list of bytes
    tx_bytes = tx_package[0].to_bytes(4, 'little', signed=True)
    for i in range(1, len(tx_package)):
        tx_bytes = tx_bytes + tx_package[i].to_bytes(4, 'little', signed=True)
    
    print(f"TX bytes data check:") #Print of values in tx_package for check:
    for i in [0, 1, 256, 257, 512, 513, 514, len(tx_package)-257]:
        val = int.from_bytes(tx_bytes[i*4:(i+1)*4], 'little', signed='True')
        print(f"Addr 0x{str(hex(i*4))[2:].zfill(8)}: {val} (0x{str(hex(val))[2:].zfill(8)})")
    print()
    
    
    
    #Send package of bytes
    print(f"Package Sending...\n")
    os.pwrite(fd_h2c, tx_bytes, TX_OFFSET)
    
    #Wait for interrupt
    os.pread(fd_event, 4, 0xfee004d8)
    os.pwrite(fd_h2c, (0xc001).to_bytes(4, 'little', signed=False), IRQACK_OFFSET)
    
    #Read package of bytes
    rx_len = data_len+256
    rx_bytes = os.pread(fd_c2h, rx_len*4, RX_OFFSET)

    
    #Transform into array from list of bytes
    rx_package = []
    for i in range(1, rx_len):
        bytes_val = rx_bytes[i*4:(i+1)*4]
        rx_package.append(int.from_bytes(bytes_val, 'little', signed="True"))
    
    #Write result to test_data file
    file_name = 'test_data'
    if (canon_en == 1):
        file_name = file_name + "_c"
    elif (trans_en == 1):
        file_name = file_name + "_t"
        
    if (warp_en == 1):
        file_name = file_name + "wH"
    else:
        file_name = file_name + "nH"
        
    if (fixed_point_en == 1):
        file_name = file_name + "f"
        
    with open(file_name + '.txt', 'w') as f:
        for i in range(0,len(rx_package)):
            f.write(f"{str(rx_package[i])}\n")
    print(file_name +" File Written\n")
        


    # done
    os.close(fd_h2c)
    os.close(fd_c2h)
    os.close(fd_event)
    
##############################################    

if __name__ == '__main__':
    main()


