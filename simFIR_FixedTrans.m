%Filtering hardware simulation for transposed FIR structures with coefficient change manager

close all force
clear

%
%% Setup
order = 256;
scale1 = 16;
scale2 = 8;
change = int32(0x52);
warped = 0;

%Create tone complex signal
Freqs = [5000 12000 19000];
Fs = 48000;
Ts = 0.015;

TimeVector = 1/Fs:1/Fs:Ts;
Signal = sin(2*pi*Freqs(1)*TimeVector);
for i = 2:length(Freqs)
    Signal = Signal + sin(2*pi*Freqs(i)*TimeVector);
end
refData = Signal;

%% Hardware Simulation Loops

%Transposed FIR
run('generateFIR.m')
impres1 = b1;
impres2 = b2;
%}

regIn = zeros(1,order);
regOut = regIn;
w = regOut;
FIR = zeros(1,length(refData)+order-1);

impres = impres1;
c = 1;

for i = 1:length(FIR)
    %First we update the incoming data to be the next sample
    if i <= length(refData)
        regIn(1) = refData(i);
    else
        regIn(1) = 0;
    end
    
    %Then we clock the system
    regOut = regIn;

    %When the registers update the system calculates the new values
    for ii = 1:order
        
        %If we are at the sample for a coefficient change, we transform the coefficient array one coefficient at a time
        if i == change*4 - order + c
            impres(order - c + 1) = impres2(order - c + 1);

            c = c + 1;
            if c > order
                c = 1;
            end
        end

        %In the transposed FIR structure, each w node is equal to the sum of the reg before it and the inverse-corresponding coefficient multiplied with regOut(1)
        w(ii) = regOut(1)*impres(order - ii + 1);

        if ii > 1
            w(ii) = w(ii) + regOut(ii);
        end
    end
    FIR(i) = w(end);
    
    %When the system is stable the register inputs are ready for the next cycle
    regIn = [regIn(1), w(1:order-1)];
end
FIRtnS = FIR;

regIn = zeros(1,order);
regOut = regIn;
w = regOut;
FIR = zeros(1,length(refData)+order-1);

samples = zeros(1, order);
temp = 0;

for i = 1:length(FIR)
    %First we update the incoming data to be the next sample
    if i <= length(refData)
        regIn(1) = refData(i);
    else
        regIn(1) = 0;
    end
    
    %Then we clock the system
    regOut = regIn;

    %When the registers update the system calculates the new values
    for ii = 1:order
        %In the transposed FIR structure, each w node is equal to the sum of the reg before it and the inverse-corresponding coefficient multiplied with regOut(1)
        if i < change*4
            w(ii) = regOut(1)*impres1(order - ii + 1);
        else
            w(ii) = regOut(1)*impres2(order - ii + 1);
        end

        if ii > 1
            %We rewrite the registers if we have a coefficient change
            if i == change*4
                temp = 0;
                %First we calculate the sum of samples with the first coefficient set this reg received excluding its incoming sample
                for iii = 1:ii - 2
                    temp = temp + samples(iii)*impres1(order - ii + iii + 1);
                end
                
                %From this we calculate the register's incoming sample
                samples(ii - 1) = (regOut(ii) - temp)/impres1(end);
                
                %Then based on the known samples we calculate the sum of samples this reg would have received with the second set of coefficients
                temp = 0;
                for iii = 1:ii - 1
                    temp = temp + samples(iii)*impres2(order - ii + iii + 1);
                end

                %We then use this sum instead of the register output
                w(ii) = w(ii) + temp;
            else
                w(ii) = w(ii) + regOut(ii);
            end
        end
    end
    FIR(i) = w(end);
    
    %When the system is stable the register inputs are ready for the next cycle
    regIn = [regIn(1), w(1:order-1)];
end
FIRtnR1 = FIR;

regIn = zeros(1,order);
regOut = regIn;
w = regOut;
FIR = zeros(1,length(refData)+order-1);

samplesTrue = flip(refData(change*4-order:change*4-1));
samples = zeros(1, order);
temp = 0;

errors = 0;
for i = 1:length(FIR)
    %First we update the incoming data to be the next sample
    if i <= length(refData)
        regIn(1) = refData(i);
    else
        regIn(1) = 0;
    end
    
    %Then we clock the system
    regOut = regIn;

    %When the registers update the system calculates the new values
    for ii = 1:order
        %In the transposed FIR structure, each w node is equal to the sum of the reg before it and the inverse-corresponding coefficient multiplied with regOut(1)
        if i < change*4
            w(ii) = regOut(1)*impres1(order - ii + 1);
        else
            w(ii) = regOut(1)*impres2(order - ii + 1);
        end

        if ii > 1
            %We rewrite the registers if we have a coefficient change
            if i == change*4
                temp = 0;
                %First we calculate the sum of samples with the first coefficient set this reg received excluding its incoming sample
                for iii = 1:ii - 2
                    temp = temp + samples(iii)*impres1(order - ii + iii + 1);
                end
                
                %From this we calculate the register's incoming sample
                samples(ii - 1) = (regOut(ii) - temp)/impres1(end);
                
                %In case we have had too many rounding issues we set the calculated sample to be the ideal result
                if (abs(samples(ii-1) - samplesTrue(ii-1)) > 10^(-3))
                    samples(ii-1) = samplesTrue(ii-1);
                    errors = errors + 1;
                end
                
                %Then based on the known samples we calculate the sum of samples this reg would have received with the second set of coefficients
                temp = 0;
                for iii = 1:ii - 1
                    temp = temp + samples(iii)*impres2(order - ii + iii + 1);
                end

                %We then use this sum instead of the register output
                w(ii) = w(ii) + temp;
            else
                w(ii) = w(ii) + regOut(ii);
            end
        end
    end
    FIR(i) = w(end);
    
    %When the system is stable the register inputs are ready for the next cycle
    regIn = [regIn(1), w(1:order-1)];
end
FIRtnR2 = FIR;

%
%% Plots
%Coefficient change marker for graphs
changeMarkX = int32(ones(1,2))*4*change;
changeMarkY = [1, -1];

load 'Test Data/fircn.mat'



figure(1)
subplot(4,1,1)
hold on;
plot(changeMarkX-(order-1),changeMarkY*4, '--', 'Color', [0 0 0])
plot(refData, 'Color', [0.0000 0.4470 0.7410])
plot(FIRtnS, 'Color', [0.8500 0.3250 0.0980])
hold off;
ylim([-4 4])
grid on;
legend('','Original Signal','Filtered Signal')
title('Filtered Signal With Coefficient Change - Transposed FIR Architecture With Coefficient-Shifting Fix')
xlabel('Sample')
ylabel('Amplitude')

subplot(4,1,2)
hold on;
plot(changeMarkX,changeMarkY*4, '--', 'Color', [0 0 0])
plot(refData, 'Color', [0.0000 0.4470 0.7410])
plot(FIRtnR1, 'Color', [0.8500 0.3250 0.0980])
hold off;
ylim([-4 4])
grid on;
legend('','Original Signal','Filtered Signal')
title('Filtered Signal With Coefficient Change - Transposed FIR Architecture With Sample-Rewriting Fix')
xlabel('Sample')
ylabel('Amplitude')

subplot(4,1,3)
hold on;
plot(changeMarkX,changeMarkY*4, '--', 'Color', [0 0 0])
plot(refData, 'Color', [0.0000 0.4470 0.7410])
plot(FIRtnR2, 'Color', [0.8500 0.3250 0.0980])
hold off;
ylim([-4 4])
grid on;
legend('','Original Signal','Filtered Signal')
title('Filtered Signal With Coefficient Change - Transposed FIR Architecture With Sample-Rewriting Fix and Sample Error Correction')
xlabel('Sample')
ylabel('Amplitude')

annotation('textbox', [0.8315, 0.265, 0.1, 0.1], 'String', "Sample Errors: " + errors,'FitBoxToText','on','BackgroundColor','w','FontSize',8)


subplot(4,1,4)
hold on;
plot(changeMarkX,changeMarkY*0.04, '--', 'Color', [0 0 0])
plot(FIRtnS - FIRc + 0.02, 'Color', [0.0000 0.4470 0.7410])
plot(FIRtnR1 - FIRc, 'Color', [0.8500 0.3250 0.0980])
plot(FIRtnR2 - FIRc - 0.02, 'Color', [0.8500 0.3250 0.0980])
hold off;
ylim([-0.04 0.04])
grid on;
legend('','Shift-Fixed Filtered Signal Difference from Ideal [+0.02]','Rewrite-Fixed Filtered Signal Difference from Ideal','Rewrite-Fixed Filtered Signal Difference from Ideal - w. Error Coerrection [-0.02]')
title('Difference Between Signal Filtered With Mixed-Coefficients Fixes and Ideal Filtering - Transposed FIR Architecture')
xlabel('Sample')
ylabel('Amplitude')

set(gcf,'position',[10,10,1600,900])
saveas(gcf, 'Fixed Transposed FIR Filters Coefficient Change Noise Graph.png')



figure(2)
hold on;
plot(changeMarkX,changeMarkY*0.04, '--','LineWidth',3, 'Color', [0 0 0])
plot(FIRtnS - FIRc + 0.02,'LineWidth',4, 'Color', [0.0000 0.4470 0.7410])
plot(FIRtnR1 - FIRc,'LineWidth',4, 'Color', [0.8500 0.3250 0.0980])
plot(FIRtnR2 - FIRc - 0.02,'LineWidth',4, 'Color', [0.8500 0.3250 0.0980])
hold off;
xlim([300 600])
ylim([-0.04 0.04])
grid on;
f = gca;
f.LineWidth = 4;
f.GridLineWidth = 2;
f.FontSize = 32;
title('Mixed-Coefficients Fixed Transposed FIR Architecture','FontSize',36)
xlabel('Sample','FontSize',32)
ylabel('Amplitude','FontSize',32)
yyaxis right
ylim([-0.04 0.04])
yticks(-0.02:0.02:0.02)
yticklabels([num2str(errors), "", ""])
ylabel('Error Rewrites')
set(gca,'YColor',[0 0 0]);

set(gcf,'position',[10,10,1800,500])
saveas(gcf, 'Fixed Transposed FIR Filters Coefficient Change Noise Subgraph.png')
%}