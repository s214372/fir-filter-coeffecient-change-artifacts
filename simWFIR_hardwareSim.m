%Filtering simulation for hardware FIR structures with integer rounding and int32 cutoff

close all force
clear

%
%% Setup
order = 256;
scale1 = 16;
scale2 = 8;
change = int32(0x52);
warped = 0;

intScale = 2^14;

%Create tone complex signal
Freqs = [5000 12000 19000];
Fs = 48000;
Ts = 0.015;

TimeVector = 1/Fs:1/Fs:Ts;
Signal = sin(2*pi*Freqs(1)*TimeVector);
for i = 2:length(Freqs)
    Signal = Signal + sin(2*pi*Freqs(i)*TimeVector);
end
refData = round(Signal*intScale,0);

%{
fileData = fopen('ref_data.txt','w');
fprintf(fileData,'%d\n',refData);
fclose(fileData);
%}

run('generateFIR.m')

impres1 = round(b1*intScale,0);
impres2 = round(b2*intScale,0);
%{
fileImp1 = fopen('coef_n_01.txt','w');
fileImp2 = fopen('coef_n_02.txt','w');
fprintf(fileImp1,'%d\n',impres1);
fprintf(fileImp2,'%d\n',impres2);
fclose(fileImp1);
fclose(fileImp2);
%}

%
%% Hardware Simulation Loops

%Canonical FIR
regIn = zeros(1,order);
regOut = regIn;
w = regOut;
FIR = zeros(1,length(refData)+order-1);

for i = 1:length(FIR)
    %First we update the incoming data to be the next sample
    if i <= length(refData)
        regIn(1) = refData(i);
    else
        regIn(1) = 0;
    end
    
    %Then we clock the system
    regOut = regIn;

    %When the registers update the system calculates the new values
    out = 0;
    for ii = 1:order
        %In the canonical FIR structure, each w node is simply equal to the corresponding register output
        w(ii) = regOut(ii);
        
        %And then we sum up all w nodes multiplied with their corresponding filter coefficient
        if i < change*4
            out = out + floor(w(ii)*impres1(ii));
        else
            out = out + floor(w(ii)*impres2(ii));
        end
    end
    FIR(i) = double(int32(out));
    
    %When the system is stable the register inputs are ready for the next cycle
    regIn = [regIn(1), w(1:order-1)];
end
FIRcnh = FIR/(intScale^2);


%Transposed FIR
regIn = zeros(1,order);
regOut = regIn;
w = regOut;
FIR = zeros(1,length(refData)+order-1);

for i = 1:length(FIR)
    %First we update the incoming data to be the next sample
    if i <= length(refData)
        regIn(1) = refData(i);
    else
        regIn(1) = 0;
    end
    
    %Then we clock the system
    regOut = regIn;

    %When the registers update the system calculates the new values
    for ii = 1:order
        %In the transposed FIR structure, each w node is equal to the sum of the reg before it and the inverse-corresponding coefficient multiplied with regOut(1)
        if i < change*4
            w(ii) = floor(regOut(1)*impres1(order - ii + 1));
        else
            w(ii) = floor(regOut(1)*impres2(order - ii + 1));
        end

        if ii > 1
            w(ii) = double(int32(w(ii) + regOut(ii)));
        end
    end
    FIR(i) = w(end);
    
    %When the system is stable the register inputs are ready for the next cycle
    regIn = [regIn(1), w(1:order-1)];
end
FIRtnh = FIR/(intScale^2);
%}

%
%Set coefficients to be warped
scale1 = 16;
scale2 = 56;
warped = 1;
lambda = 0.75;
run('generateFIR.m')

impres1 = round(b1*intScale,0);
impres2 = round(b2*intScale,0);
%{
fileImp1 = fopen('coef_w_01.txt','w');
fileImp2 = fopen('coef_w_02.txt','w');
fprintf(fileImp1,'%d\n',impres1);
fprintf(fileImp2,'%d\n',impres2);
fclose(fileImp1);
fclose(fileImp2);
%}

%
%Canonical WFIR
regIn = zeros(1,order+1);
regOut = regIn;
w = zeros(1,order);
FIR = zeros(1,length(refData)+order-1);

for i = 1:length(FIR)
    %First we update the incoming data to be the next sample
    if i <= length(refData)
        regIn(1) = refData(i);
    else
        regIn(1) = 0;
    end
    
    %Then we clock the system
    regOut = regIn;

    %When the registers update the system calculates the new values
    w(1) = regOut(1);
    out = floor(w(1)*impres1(1));
    for ii = 2:order
        %In the canonical WFIR structure, each w node is equal to:
        w(ii) = double(int32(regOut(ii)+floor(regOut(ii+1)*0.5)+floor(regOut(ii+1)*0.25)-floor(w(ii-1)*0.5)-floor(w(ii-1)*0.25)));
        
        %And then we sum up all w nodes multiplied with their corresponding filter coefficient
        if i < change*4
            out = out + floor(w(ii)*impres1(ii));
        else
            out = out + floor(w(ii)*impres2(ii));
        end
    end
    FIR(i) = double(int32(out));
    
    %When the system is stable the register inputs are ready for the next cycle
    regIn = [regIn(1), w(1:order)];
end
FIRcwh = FIR/(intScale^2);


%Transposed WFIR
regIn = zeros(1,order+1);
regOut = regIn;
w = zeros(1,order);
FIR = zeros(1,length(refData)+order-1);

for i = 1:length(FIR)
    %First we update the incoming data to be the next sample
    if i <= length(refData)
        regIn(1) = refData(i);
    else
        regIn(1) = 0;
    end
    
    %Then we clock the system
    regOut = regIn;

    %When the registers update the system calculates the new values
    for ii = 1:order
        %In the transposed WFIR structure, each w node is equal to the sum of the reg before it and the inverse-corresponding coefficient multiplied with regOut(1)
        if i < change*4
            w(ii) = double(int32(floor(regOut(1)*impres1(order - ii + 1))));
        else
            w(ii) = double(int32(floor(regOut(1)*impres2(order - ii + 1))));
        end

        if ii > 1
            w(ii) = double(int32(w(ii) + regOut(ii)+floor(regOut(ii+1)*0.5)+floor(regOut(ii+1)*0.25)-floor(w(ii-1)*0.5)-floor(w(ii-1)*0.25)));
        end
    end
    FIR(i) = w(end);
    
    %When the system is stable the register inputs are ready for the next cycle
    regIn = [regIn(1), w];
end
FIRtwh = FIR/(intScale^2);
%}

%{
%% Hex conversion for comparison during development
FIRcnh = dec2hex(FIRcnh,8);
FIRcwh = dec2hex(FIRcwh,8);
FIRtnh = dec2hex(FIRtnh,8);
FIRtwh = dec2hex(FIRtwh,8);
%}

%
%% Loading of hardware results from files into Matlab
fileFIRcnH = fopen('Test Data/test_data_cnH.txt','r');
fileFIRcwH = fopen('Test Data/test_data_cwH.txt','r');
fileFIRtnH = fopen('Test Data/test_data_tnH.txt','r');
fileFIRtwH = fopen('Test Data/test_data_twH.txt','r');
fileFIRcw2H = fopen('Test Data/test_data_cw2H.txt','r');
FIRcnH = transpose(fscanf(fileFIRcnH, '%d')/(intScale^2));
FIRcwH = transpose(fscanf(fileFIRcwH, '%d')/(intScale^2));
FIRtnH = transpose(fscanf(fileFIRtnH, '%d')/(intScale^2));
FIRtwH = transpose(fscanf(fileFIRtwH, '%d')/(intScale^2));
FIRcw2H = transpose(fscanf(fileFIRcw2H, '%d')/(intScale^2));
fclose(fileFIRcnH);
fclose(fileFIRcwH);
fclose(fileFIRtnH);
fclose(fileFIRtwH);
fclose(fileFIRcw2H);
%}

%
%% Plots
%Canonical WFIR with only coefs_02 to compare
regIn = zeros(1,order+1);
regOut = regIn;
w = zeros(1,order);
FIR = zeros(1,length(refData)+order-1);

for i = 1:length(FIR)
    %First we update the incoming data to be the next sample
    if i <= length(refData)
        regIn(1) = refData(i);
    else
        regIn(1) = 0;
    end
    
    %Then we clock the system
    regOut = regIn;

    %When the registers update the system calculates the new values
    w(1) = regOut(1);
    out = floor(w(1)*impres1(1));
    for ii = 2:order
        %In the canonical WFIR structure, each w node is equal to:
        w(ii) = double(int32(regOut(ii)+floor(regOut(ii+1)*0.5)+floor(regOut(ii+1)*0.25)-floor(w(ii-1)*0.5)-floor(w(ii-1)*0.25)));
        
        %And then we sum up all w nodes multiplied with their corresponding filter coefficient
        out = out + floor(w(ii)*impres2(ii));
    end
    FIR(i) = double(int32(out));
    
    %When the system is stable the register inputs are ready for the next cycle
    regIn = [regIn(1), w(1:order)];
end
FIRcw2h = FIR/(intScale^2);

%Coefficient change marker for graphs
changeMarkX = int32(ones(1,2))*int32(4*change);
changeMarkY = [1, -1];

load 'Test Data/fircn.mat'
load 'Test Data/firtn.mat'
FIRcn = FIRc;
FIRtn = FIRt;

load 'Test Data/fircw.mat'
load 'Test Data/firtw.mat'
FIRcw = FIRc;
FIRtw = FIRt;

if intScale == 2^0
    yLim = 40;
    CWFIR = 10;
elseif intScale == 2^3
    yLim = 4;
    CWFIR = 10;
elseif intScale == 2^10
    yLim = 0.4;
    CWFIR = 10;
else
    yLim = 20;
    CWFIR = 0.01;
end

%
figure(1)
fontSize = 9;

subplot(4,1,1)
hold on;
plot(changeMarkX,changeMarkY*yLim, '--', 'Color', [0 0 0])
plot(FIRcn - FIRcnh)
hold off;
ylim([-yLim yLim])
grid on;
leg = legend('Coefficient Change Mark','Matlab - Hardware Simulated Signal');
set(leg, 'FontSize', fontSize);
title('Comparison Between Filtered Signals With Coefficient Change - Canonical FIR Architecture')
xlabel('Sample')
ylabel('Amplitude')

subplot(4,1,2)
hold on;
plot(changeMarkX,changeMarkY*yLim, '--', 'Color', [0 0 0])
plot(FIRtn - FIRtnh)
hold off;
ylim([-yLim yLim])
grid on;
leg = legend('Coefficient Change Mark','Matlab - Hardware Simulated Signal');
set(leg, 'FontSize', fontSize);
title('Comparison Between Filtered Signals With Coefficient Change - Transposed FIR Architecture')
xlabel('Sample')
ylabel('Amplitude')

subplot(4,1,3)
hold on;
plot(changeMarkX,changeMarkY*yLim*CWFIR, '--', 'Color', [0 0 0])
plot(FIRcw - FIRcwh)
hold off;
ylim([-yLim*CWFIR yLim*CWFIR])
grid on;
leg = legend('Coefficient Change Mark','Matlab - Hardware Simulated Signal');
set(leg, 'FontSize', fontSize);
title('Comparison Between Filtered Signals With Coefficient Change - Canonical WFIR Architecture')
xlabel('Sample')
ylabel('Amplitude')

subplot(4,1,4)
hold on;
plot(changeMarkX,changeMarkY*yLim, '--', 'Color', [0 0 0])
plot(FIRtw - FIRtwh)
hold off;
ylim([-yLim yLim])
grid on;
leg = legend('Coefficient Change Mark','Matlab - Hardware Simulated Signal');
set(leg, 'FontSize', fontSize);
title('Comparison Between Filtered Signals With Coefficient Change - Transposed WFIR Architecture')
xlabel('Sample')
ylabel('Amplitude')

set(gcf,'position',[10,10,900,1200])
saveas(gcf, strcat("Matlab vs Hardware Coefficient Change Noise Graph - With ", num2str(intScale, '%d'), "x Signal Scaling.png"))
%}

%
figure(2)
yLim = 0.4;

subplot(4,1,1)
hold on;
plot(changeMarkX,changeMarkY*yLim, '--', 'Color', [0 0 0])
plot(FIRcnh - FIRcnH)
hold off;
ylim([-yLim yLim])
grid on;
legend('Coefficient Change Mark','Hardware Simulated - Hardware Filtered Signal')
title('Comparison Between Filtered Signals With Coefficient Change - Canonical FIR Architecture')
xlabel('Sample')
ylabel('Amplitude')

subplot(4,1,2)
hold on;
plot(changeMarkX,changeMarkY*yLim, '--', 'Color', [0 0 0])
plot(FIRtnh - FIRtnH)
hold off;
ylim([-yLim yLim])
grid on;
legend('Coefficient Change Mark','Hardware Simulated - Hardware Filtered Signal')
title('Comparison Between Filtered Signals With Coefficient Change - Transposed FIR Architecture')
xlabel('Sample')
ylabel('Amplitude')

subplot(4,1,3)
hold on;
plot(changeMarkX,changeMarkY*yLim, '--', 'Color', [0 0 0])
plot(FIRcwh - FIRcwH)
hold off;
ylim([-yLim yLim])
grid on;
legend('Coefficient Change Mark','Hardware Simulated - Hardware Filtered Signal')
title('Comparison Between Filtered Signals With Coefficient Change - Canonical WFIR Architecture')
xlabel('Sample')
ylabel('Amplitude')

subplot(4,1,4)
hold on;
plot(changeMarkX,changeMarkY*yLim, '--', 'Color', [0 0 0])
plot(FIRtwh - FIRtwH)
hold off;
ylim([-yLim yLim])
grid on;
legend('Coefficient Change Mark','Hardware Simulated - Hardware Filtered Signal')
title('Comparison Between Filtered Signals With Coefficient Change - Transposed WFIR Architecture')
xlabel('Sample')
ylabel('Amplitude')

set(gcf,'position',[10,10,900,700])
saveas(gcf, strcat("Matlab Hardware vs FPGA Hardware Coefficient Change Noise Graph.png"))



figure(3)
yLim = 0.12;

hold on;
plot(changeMarkX,changeMarkY*yLim, '--', 'Color', [0 0 0])
plot(FIRcwh - FIRcwH + 0.05)
plot(FIRcw2h - FIRcw2H, 'Color', [1 0.5 0.1])
plot([refData/100000 zeros(1,256)] - 0.05, 'Color', [0.1 0.5 1])
hold off;
ylim([-yLim yLim])
grid on;
legend('Coefficient Change Mark','Hardware Filtered Signal Difference [+0.05]', 'Hardware Filtered Coefs_2 Difference', 'Input Signal [1/10^5-0.05]')
title('Comparison Between Filtered Signals With Coefficient Change and Input Signal - Canonical WFIR Architecture')
xlabel('Sample')
ylabel('Amplitude')

set(gcf,'position',[10,10,1600,400])
saveas(gcf, strcat("Canonical WFIR Coef2 Bleedthrough Noise Graph.png"))


%
figure(4)

B = 32;
dif = 0.1;
hold on;

b = 0;
ppScale = mag2db(2^((B*2)-(b*2)-1));
pScale = mag2db(2^(B-(b*2)-1));
nScale = mag2db(1/(2^(b*2)));
plotX = [ppScale, ppScale, ppScale, pScale, pScale, pScale, pScale, nScale, nScale, nScale];
plotY = [1-dif, 1+dif, 1, 1, 1-dif, 1+dif, 1, 1, 1-dif, 1+dif];
plot(plotX, plotY+1.45, '--', 'Color', [0.4 0.4 0.4])
pScale = mag2db(2^(B-b-1));
nScale = mag2db(1/(2^b));
plotX = [pScale, pScale, pScale, 0, 0, 0, 0, nScale, nScale, nScale];
plotY = [1-dif, 1+dif, 1, 1, 1-(dif*2), 1+dif, 1, 1, 1-dif, 1+dif];
plot(plotX, plotY+1.5, 'Color', [0 0 0])

b = 3;
ppScale = mag2db(2^((B*2)-(b*2)-1));
pScale = mag2db(2^(B-(b*2)-1));
nScale = mag2db(1/(2^(b*2)));
plotX = [ppScale, ppScale, ppScale, pScale, pScale, pScale, pScale, nScale, nScale, nScale];
plotY = [1-dif, 1+dif, 1, 1, 1-dif, 1+dif, 1, 1, 1-dif, 1+dif];
plot(plotX, plotY+0.95, '--', 'Color', [0.4 0.4 0.4])
pScale = mag2db(2^(B-b-1));
nScale = mag2db(1/(2^b));
plotX = [pScale, pScale, pScale, 0, 0, 0, 0, nScale, nScale, nScale];
plotY = [1-dif, 1+dif, 1, 1, 1-(dif*2), 1+dif, 1, 1, 1-dif, 1+dif];
plot(plotX, plotY+1.0, 'Color', [0 0 0])

b = 10;
ppScale = mag2db(2^((B*2)-(b*2)-1));
pScale = mag2db(2^(B-(b*2)-1));
nScale = mag2db(1/(2^(b*2)));
plotX = [ppScale, ppScale, ppScale, pScale, pScale, pScale, pScale, nScale, nScale, nScale];
plotY = [1-dif, 1+dif, 1, 1, 1-dif, 1+dif, 1, 1, 1-dif, 1+dif];
plot(plotX, plotY+0.45, '--', 'Color', [0.4 0.4 0.4])
pScale = mag2db(2^(B-b-1));
nScale = mag2db(1/(2^b));
plotX = [pScale, pScale, pScale, 0, 0, 0, 0, nScale, nScale, nScale];
plotY = [1-dif, 1+dif, 1, 1, 1-(dif*2), 1+dif, 1, 1, 1-dif, 1+dif];
plot(plotX, plotY+0.5, 'Color', [0 0 0])

b = 14;
ppScale = mag2db(2^((B*2)-(b*2)-1));
pScale = mag2db(2^(B-(b*2)-1));
nScale = mag2db(1/(2^(b*2)));
plotX = [ppScale, ppScale, ppScale, pScale, pScale, pScale, pScale, nScale, nScale, nScale];
plotY = [1-dif, 1+dif, 1, 1, 1-dif, 1+dif, 1, 1, 1-dif, 1+dif];
plot(plotX, plotY-0.05, '--', 'Color', [0.4 0.4 0.4])
pScale = mag2db(2^(B-b-1));
nScale = mag2db(1/(2^b));
plotX = [pScale, pScale, pScale, 0, 0, 0, 0, nScale, nScale, nScale];
plotY = [1-dif, 1+dif, 1, 1, 1-(dif*2), 1+dif, 1, 1, 1-dif, 1+dif];
plot(plotX, plotY, 'Color', [0 0 0])

hold off;

xticks(-200:50:400)
xlabel("dB Magnitude Limits of 32 Bit Values")
ylabel("Fixed Point Bit Precision")
yticks([1, 1.5, 2, 2.5])
ylim([0.5 3.25])
legend('', '32-bit values', '64-bit multiplication result values, with 32-bit cutoff indicated')
yticklabels(["14 bit", "10 bit", "3 bit", "0 bit"])
set(gcf,'position',[10,10,1200,500])
saveas(gcf, strcat("Fixed Point Magnitude Limits.png"))

%}