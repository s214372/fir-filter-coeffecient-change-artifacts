clear

lambda = 0.75;

bw = [1 lambda];
aw = [lambda 1];
[h1, w1] = freqz(bw, aw);

bw = [1 -lambda];
aw = [-lambda 1];
[h2, w2] = freqz(bw, aw);

figure(1)
subplot(2,2,1)
plot(w1/pi, mag2db(abs(h1)))
ylim([-1 1])
grid on;
title('Bode Plot for Synthesis Allpass Filter')
xlabel('Normalized Frequency')
ylabel('Magnitude [dB]')

subplot(2,2,2)
plot(w2/pi, mag2db(abs(h2)), "Color", [0.8500 0.3250 0.0980])
ylim([-1 1])
grid on;
title('Bode Plot for Analysis Allpass Filter')
xlabel('Normalized Frequency')
ylabel('Magnitude [dB]')

subplot(2,2,[3 4])
hold on;
plot(w1/pi, angle(h1)*180/pi)
plot(w2/pi, angle(h2)*180/pi)
grid on;
ylim([0 180])
yticks([-180 -150 -120 -90 -60 -30 0 30 60 90 120 150 180])
hold off;
legend("Synthesis Allpass Filter", "Analysis Allpass Filter")
title('Combined Phase Plot for Allpass Filters')
xlabel('Normalized Frequency')
ylabel('Phase [deg]')

set(gcf,'position',[10,10,2000,1000])
saveas(gcf, "Warping Allpass Filters' Bode Plots.png")
