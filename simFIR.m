%Filtering hardware simulation for normal FIR structures

close all force
clear

%% Setup
order = 256;
scale1 = 16;
scale2 = 8;
change = int32(0x52);
warped = 0;

%Create tone complex signal
Freqs = [5000 12000 19000];
Fs = 48000;
Ts = 0.015; %2; %

TimeVector = 1/Fs:1/Fs:Ts;
Signal = sin(2*pi*Freqs(1)*TimeVector);
for i = 2:length(Freqs)
    Signal = Signal + sin(2*pi*Freqs(i)*TimeVector);
end
refData = Signal;

if length(refData) > 0x800
    change = change*int32(0x80);
end

%Generate impulse responses for filtering
run('generateFIR.m')
impres1 = b1;
impres2 = b2;


%% Hardware Simulation Loops

%Canonical FIR
regIn = zeros(1,order);
regOut = regIn;
w = regOut;
FIR = zeros(1,length(refData)+order-1);

for i = 1:length(FIR)
    %First we update the incoming data to be the next sample
    if i <= length(refData)
        regIn(1) = refData(i);
    else
        regIn(1) = 0;
    end
    
    %Then we clock the system
    regOut = regIn;

    %When the registers update the system calculates the new values
    out = 0;
    for ii = 1:order
        %In the canonical FIR structure, each w node is simply equal to the corresponding register output
        w(ii) = regOut(ii);
        
        %And then we sum up all w nodes multiplied with their corresponding filter coefficient
        if i < change*4
            out = out + w(ii)*impres1(ii);
        else
            out = out + w(ii)*impres2(ii);
        end
    end
    FIR(i) = out;
    
    %When the system is stable the register inputs are ready for the next cycle
    regIn = [regIn(1), w(1:order-1)];
end
FIRc = FIR;


%Transposed FIR
regIn = zeros(1,order);
regOut = regIn;
w = regOut;
FIR = zeros(1,length(refData)+order-1);

for i = 1:length(FIR)
    %First we update the incoming data to be the next sample
    if i <= length(refData)
        regIn(1) = refData(i);
    else
        regIn(1) = 0;
    end
    
    %Then we clock the system
    regOut = regIn;

    %When the registers update the system calculates the new values
    for ii = 1:order
        %In the transposed FIR structure, each w node is equal to the sum of the reg before it and the inverse-corresponding coefficient multiplied with regOut(1)
        if i < change*4
            w(ii) = regOut(1)*impres1(order - ii + 1);
        else
            w(ii) = regOut(1)*impres2(order - ii + 1);
        end

        if ii > 1
            w(ii) = w(ii) + regOut(ii);
        end
    end
    FIR(i) = w(end);
    
    %When the system is stable the register inputs are ready for the next cycle
    regIn = [regIn(1), w(1:order-1)];
end
FIRt = FIR;

%Maintain basic coefficients for plotting
coef1 = b1*scale1;
coef2 = b2*scale2;


%Lattice FIR
regIn = zeros(1,order+1);
regOut = regIn;
w = regOut;
v = regIn;
k1 = poly2rc(filter(b1,1,[1 zeros(1,order)]));
k2 = poly2rc(filter(b2,1,[1 zeros(1,order)]));
scale1 = 30; %Arbitrary?
scale2 = 16; %Arbitrary?
FIR = zeros(1,length(refData)+order-1);

for i = 1:length(FIR)
    %First we update the incoming data to be the next sample
    if i <= length(refData)
        regIn(1) = refData(i);
    else
        regIn(1) = 0;
    end
    
    %Then we clock the system
    regOut = regIn;

    %When the registers update the system calculates the new values
    v(1) = regOut(1);
    w(1) = regOut(1);
    for ii = 2:order+1
        %In the lattice FIR structure, each w node is equal to 
        if i < change*4
            v(ii) = v(ii-1) + k1(ii-1)*regOut(ii);
            w(ii) = regOut(ii) + k1(ii-1)*v(ii-1);
        else
            v(ii) = v(ii-1) + k2(ii-1)*regOut(ii);
            w(ii) = regOut(ii) + k2(ii-1)*v(ii-1);
        end
    end

    if i < change*4
        FIR(i) = v(end)/scale1;
    else
        FIR(i) = v(end)/scale2;
    end
        
    %When the system is stable the register inputs are ready for the next cycle
    regIn = [regIn(1), w(1:order)];
end
FIRk = FIR;


%% Filtering for Comparison

%Normal convolution filtering
IR1 = conv(impres1,refData);
IR2 = conv(impres2,refData);
if change*4 < length(IR1)
    IR = [IR1(1:4*change-1) IR2(4*change:end)];
else
    IR = IR1;
end

%Lattice filtering
IRk1 = latcfilt(k1, [refData zeros(1,order-1)])/scale1;
IRk2 = latcfilt(k2, [refData zeros(1,order-1)])/scale2;
if change*4 < length(IRk1)
    IRk = [IRk1(1:4*change) IRk2(4*change+1:end)];
else
    IRk = IRk1;
end

save 'Test Data/fircn.mat' FIRc
save 'Test Data/firtn.mat' FIRt
save 'Test Data/imp1.mat' impres1
save 'Test Data/imp2.mat' impres2


%
%% Plots
%Coefficient change marker for graphs
changeMarkX = int32(ones(1,2))*4*change;
changeMarkY = [1, -1];
%
figure(1)

subplot(6,1,1)
hold on;
plot(refData)
plot(FIRc)
hold off;
ylim([-4 4])
grid on;
legend('Original Signal','Filtered Signal')
title('Filtered Signal With Coefficient Change - Canonical FIR Architecture')
xlabel('Sample')
ylabel('Amplitude')

subplot(6,1,2)
hold on;
plot(changeMarkX,changeMarkY*4, '--', 'Color', [0 0 0])
plot(IR - FIRc)
hold off;
ylim([-4 4])
grid on;
legend('Filter Coefficient Change Mark','Filtered Signal Difference from Ideal')
title('Difference Between Ideal Filtered Signal and Result - Canonical FIR Architecture')
xlabel('Sample')
ylabel('Amplitude')

subplot(6,1,3)
hold on;
plot(refData)
plot(FIRt)
hold off;
ylim([-4 4])
grid on;
legend('Original Signal','Filtered Signal')
title('Filtered Signal With Coefficient Change - Transposed FIR Architecture')
xlabel('Sample')
ylabel('Amplitude')

subplot(6,1,4)
hold on;
plot(changeMarkX,changeMarkY*40, '--', 'Color', [0 0 0])
plot(IR - FIRt)
hold off;
ylim([-40 40])
grid on;
legend('Filter Coefficient Change Mark','Filtered Signal Difference from Ideal')
title('Difference Between Ideal Filtered Signal and Result - Transposed FIR Architecture')
xlabel('Sample')
ylabel('Amplitude')

subplot(6,1,5)
hold on;
plot(refData)
plot(FIRk)
hold off;
ylim([-4 4])
grid on;
legend('Original Signal','Filtered Signal')
title('Filtered Signal With Coefficient Change - Lattice FIR Architecture')
xlabel('Sample')
ylabel('Amplitude')

subplot(6,1,6)
hold on;
plot(changeMarkX,changeMarkY*600, '--', 'Color', [0 0 0])
plot(IRk - FIRk)
hold off;
ylim([-600 600])
grid on;
legend('Filter Coefficient Change Mark','Filtered Signal Difference from Ideal')
title('Difference Between Ideal Filtered Signal and Result - Lattice FIR Architecture')
xlabel('Sample')
ylabel('Amplitude')

set(gcf,'position',[10,10,900,1000])
saveas(gcf, 'FIR Coefficient Change Noise Graph.png')



figure(2)

subplot(5,1,1)
data = refData;
L = length(data);
FFT = fft(data);
FFT = fftshift(FFT);
freq = (-L/2:L/2-1)*(Fs/L);
mag = mag2db((abs(FFT).^2)/L);
plot(freq(ceil(length(freq)/2)+1:end),mag(ceil(length(freq)/2)+1:end))
ylim([-600 200])
xlim([0 Fs/2])
xticks(0:4000:Fs/2)
xticklabels(string(0:4:Fs/2000))
grid on;
title('FFT of Original Signal')
xlabel('Frequency [kHz]')
ylabel('Magnitude [dB]')

subplot(5,1,2)
hold on;
data = FIRc(order:end-order);
L = length(data);
FFT = fft(data);
FFT = fftshift(FFT);
freq = (-L/2:L/2-1)*(Fs/L);
mag = mag2db((abs(FFT).^2)/L);
plot(freq(ceil(length(freq)/2)+1:end),mag(ceil(length(freq)/2)+1:end))
data = FIRc(order:4*change);
L = length(data);
FFT = fft(data);
FFT = fftshift(FFT);
freq = (-L/2:L/2-1)*(Fs/L);
mag = mag2db((abs(FFT).^2)/L);
plot(freq(ceil(length(freq)/2)+1:end),mag(ceil(length(freq)/2)+1:end))
replot1 = freq(ceil(length(freq)/2)+1:end);
replot2 = mag(ceil(length(freq)/2)+1:end);
data = FIRc(4*change+order:end-order);
L = length(data);
FFT = fft(data);
FFT = fftshift(FFT);
freq = (-L/2:L/2-1)*(Fs/L);
mag = mag2db((abs(FFT).^2)/L);
plot(freq(ceil(length(freq)/2)+1:end),mag(ceil(length(freq)/2)+1:end), "Color", [0 0.7 0.2])
plot(replot1(floor(length(replot1)*0.4):floor(length(replot1)*0.6)),replot2(floor(length(replot2)*0.4):floor(length(replot2)*0.6)), "Color", [0.8500 0.3250 0.0980])
hold off;
ylim([-200 200])
xlim([0 Fs/2])
xticks(0:4000:Fs/2)
xticklabels(string(0:4:Fs/2000))
grid on;
legend('Signal With Change','Before Change','After Change')
title('FFT of Filtered Signal - Canonical FIR Architecture')
xlabel('Frequency [kHz]')
ylabel('Magnitude [dB]')

subplot(5,1,3)
hold on;
data = FIRt(order:end-order);
L = length(data);
FFT = fft(data);
FFT = fftshift(FFT);
freq = (-L/2:L/2-1)*(Fs/L);
mag = mag2db((abs(FFT).^2)/L);
plot(freq(ceil(length(freq)/2)+1:end),mag(ceil(length(freq)/2)+1:end))
data = FIRt(order:4*change);
L = length(data);
FFT = fft(data);
FFT = fftshift(FFT);
freq = (-L/2:L/2-1)*(Fs/L);
mag = mag2db((abs(FFT).^2)/L);
plot(freq(ceil(length(freq)/2)+1:end),mag(ceil(length(freq)/2)+1:end))
replot1 = freq(ceil(length(freq)/2)+1:end);
replot2 = mag(ceil(length(freq)/2)+1:end);
data = FIRt(4*change+order:end-order);
L = length(data);
FFT = fft(data);
FFT = fftshift(FFT);
freq = (-L/2:L/2-1)*(Fs/L);
mag = mag2db((abs(FFT).^2)/L);
plot(freq(ceil(length(freq)/2)+1:end),mag(ceil(length(freq)/2)+1:end), "Color", [0 0.7 0.2])
plot(replot1(floor(length(replot1)*0.4):floor(length(replot1)*0.6)),replot2(floor(length(replot2)*0.4):floor(length(replot2)*0.6)), "Color", [0.8500 0.3250 0.0980])
hold off;
ylim([-200 200])
xlim([0 Fs/2])
xticks(0:4000:Fs/2)
xticklabels(string(0:4:Fs/2000))
grid on;
legend('Signal With Change','Before Change','After Change')
title('FFT of Filtered Signal - Transposed FIR Architecture')
xlabel('Frequency [kHz]')
ylabel('Magnitude [dB]')

subplot(5,1,4)
hold on;
data = FIRk(order:end-order);
L = length(data);
FFT = fft(data);
FFT = fftshift(FFT);
freq = (-L/2:L/2-1)*(Fs/L);
mag = mag2db((abs(FFT).^2)/L);
plot(freq(ceil(length(freq)/2)+1:end),mag(ceil(length(freq)/2)+1:end))
data = FIRk(order:4*change);
L = length(data);
FFT = fft(data);
FFT = fftshift(FFT);
freq = (-L/2:L/2-1)*(Fs/L);
mag = mag2db((abs(FFT).^2)/L);
plot(freq(ceil(length(freq)/2)+1:end),mag(ceil(length(freq)/2)+1:end))
replot1 = freq(ceil(length(freq)/2)+1:end);
replot2 = mag(ceil(length(freq)/2)+1:end);
data = FIRk(4*change+order:end-order);
L = length(data);
FFT = fft(data);
FFT = fftshift(FFT);
freq = (-L/2:L/2-1)*(Fs/L);
mag = mag2db((abs(FFT).^2)/L);
plot(freq(ceil(length(freq)/2)+1:end),mag(ceil(length(freq)/2)+1:end), "Color", [0 0.7 0.2])
plot(replot1(floor(length(replot1)*0.4):floor(length(replot1)*0.6)),replot2(floor(length(replot2)*0.4):floor(length(replot2)*0.6)), "Color", [0.8500 0.3250 0.0980])
hold off;
ylim([-200 200])
xlim([0 Fs/2])
xticks(0:4000:Fs/2)
xticklabels(string(0:4:Fs/2000))
grid on;
legend('Signal With Change','Before Change','After Change')
title('FFT of Filtered Signal - Lattice FIR Architecture')
xlabel('Frequency [kHz]')
ylabel('Magnitude [dB]')

subplot(5,1,5)
hold on;
L = length(impres1);
FFT = fft(impres1);
FFT = fftshift(FFT);
freq = (-L/2:L/2-1)*(Fs/L);
mag = mag2db((abs(FFT).^2)/L);
plot(freq(ceil(length(freq)/2)+1:end),mag(ceil(length(freq)/2)+1:end))
L = length(impres2);
FFT = fft(impres2);
FFT = fftshift(FFT);
freq = (-L/2:L/2-1)*(Fs/L);
mag = mag2db((abs(FFT).^2)/L);
plot(freq(ceil(length(freq)/2)+1:end),mag(ceil(length(freq)/2)+1:end))
hold off;
ylim([-200 200])
xlim([0 Fs/2])
xticks(0:4000:Fs/2)
xticklabels(string(0:4:Fs/2000))
grid on;
legend('Coefficient Set 1','Coefficient Set 2')
title('FFT of Impulse Responses')
xlabel('Frequency [kHz]')
ylabel('Magnitude [dB]')

set(gcf,'position',[600,10,1200,1000])
saveas(gcf, 'FIR Coefficient Change FFTs.png')



figure(3)

subplot(2,3,1)
hold on;
zplane(coef1,1)
fplot(@(t) sin(t), @(t) cos(t), '-', 'Color', [0 0 0]);
hold off;
%grid on;
xlim([-1.2 1.2])
ylim([-1.2 1.2])
title('Pole-Zero Plot - Coefficient Set 1')

subplot(2,3,[2 3])
stem(coef1,".")
xlim([1 order])
ylim([-150 150])
title('Coefficient Values - Coefficient Set 1')
xlabel('Sample')
ylabel('Amplitude')

subplot(2,3,4)
hold on;
zplane(coef2,1)
fplot(@(t) sin(t), @(t) cos(t), '-', 'Color', [0 0 0]);
hold off;
%grid on;
xlim([-1.2 1.2])
ylim([-1.2 1.2])
title('Pole-Zero Plot - Coefficient Set 2')

subplot(2,3,[5 6])
stem(coef2,".")
xlim([1 order])
ylim([-150 150])
title('Coefficient Values - Coefficient Set 2')
xlabel('Sample')
ylabel('Amplitude')

set(gcf,'position',[10,10,1200,660])
saveas(gcf, 'FIR Coefficients Plot.png')



figure(4)

hold on;
plot(changeMarkX,changeMarkY*40, '--','LineWidth',3, 'Color', [0 0 0])
plot(IR - FIRt,'LineWidth',4)
hold off;
xlim([300 600])
ylim([-30 30])
grid on;
f = gca;
f.LineWidth = 4;
f.GridLineWidth = 2;
f.FontSize = 32;
title('Mixed-Coefficients Artifact in the Transposed FIR Architecture','FontSize',36)
xlabel('Sample','FontSize',32)
ylabel('Amplitude','FontSize',32)

set(gcf,'position',[10,10,1800,500])
saveas(gcf, 'FIR Coefficient Change Noise Subgraph.png')



figure(5)
hold on;

data = FIRc(order:end-order);
L = length(data);
FFT = fft(data);
FFT = fftshift(FFT);
freq = (-L/2:L/2-1)*(Fs/L);
mag = mag2db((abs(FFT).^2)/L);
plot(freq(ceil(length(freq)/2)+1:end),mag(ceil(length(freq)/2)+1:end),'LineWidth',6)

data = FIRc(order:4*change);
L = length(data);
FFT = fft(data);
FFT = fftshift(FFT);
freq = (-L/2:L/2-1)*(Fs/L);
mag = mag2db((abs(FFT).^2)/L);
plot(freq(ceil(length(freq)/2)+1:end),mag(ceil(length(freq)/2)+1:end),'LineWidth',4)

replot1 = freq(ceil(length(freq)/2)+1:end);
replot2 = mag(ceil(length(freq)/2)+1:end);

data = FIRc(4*change+order:end-order);
L = length(data);
FFT = fft(data);
FFT = fftshift(FFT);
freq = (-L/2:L/2-1)*(Fs/L);
mag = mag2db((abs(FFT).^2)/L);
plot(freq(ceil(length(freq)/2)+1:end),mag(ceil(length(freq)/2)+1:end),'LineWidth',4, "Color", [0 0.7 0.2])

plot(replot1(floor(length(replot1)*0.4):floor(length(replot1)*0.6)),replot2(floor(length(replot2)*0.4):floor(length(replot2)*0.6)),'LineWidth',4, "Color", [0.8500 0.3250 0.0980])

hold off;
xlim([0 Fs/2])
xticks(0:4000:Fs/2)
xticklabels(string(0:4:Fs/2000))
ylim([-150 150])
grid on;
f = gca;
f.LineWidth = 4;
f.GridLineWidth = 2;
f.FontSize = 32;
title('Frequency-Step Artifact in the Canonical FIR Architecture','FontSize',36)
xlabel('Frequency [kHz]','FontSize',32)
ylabel('Magnitude [dB]','FontSize',32)

set(gcf,'position',[10,10,1800,500])
saveas(gcf, 'FIR Coefficient Change FFTs Subgraph.png')


%}