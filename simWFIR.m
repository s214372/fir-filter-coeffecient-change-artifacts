%Filtering hardware simulation for warped FIR structures

close all force
clear

%% Setup
order = 256;
scale1 = 16;
scale2 = 56;
change = int32(0x52);
warped = 1;
lambda = 0.75;

%Create tone complex signal
Freqs = [5000 12000 19000]; %[100:5:23900]; %
Fs = 48000;
Ts = 0.015; %2; %

TimeVector = 1/Fs:1/Fs:Ts;
Signal = sin(2*pi*Freqs(1)*TimeVector);
for i = 2:length(Freqs)
    Signal = Signal + sin(2*pi*Freqs(i)*TimeVector);
end
refData = Signal;

if length(refData) > 0x800
    change = change*int32(0x80);
end

run('generateFIR.m')

impres1 = b1;
impres2 = b2;


%% Hardware Simulation Loops

%Canonical WFIR
regIn = zeros(1,order+1);
regOut = regIn;
w = zeros(1,order);
FIR = zeros(1,length(refData)+order-1);

for i = 1:length(FIR)
    %First we update the incoming data to be the next sample
    if i <= length(refData)
        regIn(1) = refData(i);
    else
        regIn(1) = 0;
    end
    
    %Then we clock the system
    regOut = regIn;

    %When the registers update the system calculates the new values
    w(1) = regOut(1);
    out = w(1)*impres1(1);
    for ii = 2:order
        %In the canonical WFIR structure, each w node is equal to:
        w(ii) = regOut(ii)+(regOut(ii+1)-w(ii-1))*lambda;
        
        %And then we sum up all w nodes multiplied with their corresponding filter coefficient
        if i < change*4
            out = out + w(ii)*impres1(ii);
        else
            out = out + w(ii)*impres2(ii);
        end
    end
    FIR(i) = out;
    
    %When the system is stable the register inputs are ready for the next cycle
    regIn = [regIn(1), w(1:order)];
end
FIRc = FIR;


%Transposed WFIR
regIn = zeros(1,order+1);
regOut = regIn;
w = zeros(1,order);
FIR = zeros(1,length(refData)+order-1);

for i = 1:length(FIR)
    %First we update the incoming data to be the next sample
    if i <= length(refData)
        regIn(1) = refData(i);
    else
        regIn(1) = 0;
    end
    
    %Then we clock the system
    regOut = regIn;

    %When the registers update the system calculates the new values
    for ii = 1:order
        %In the transposed WFIR structure, each w node is equal to the sum of the reg before it and the inverse-corresponding coefficient multiplied with regOut(1)
        if i < change*4
            w(ii) = regOut(1)*impres1(order - ii + 1);
        else
            w(ii) = regOut(1)*impres2(order - ii + 1);
        end

        if ii > 1
            w(ii) = w(ii) + regOut(ii)+(regOut(ii+1)-w(ii-1))*lambda;
        end
    end
    FIR(i) = w(end);
    
    %When the system is stable the register inputs are ready for the next cycle
    regIn = [regIn(1), w];
end
FIRt = FIR;


%Lattice WFIR

%Create lattice coefficients
impres1 = filter(b1,1,[1 zeros(1,order)]);
impres2 = filter(b2,1,[1 zeros(1,order)]);
k1 = poly2rc(impres1);
k2 = poly2rc(impres2);

%Hand-tuned scaling factors for concise comparison with original signal
scale1 = 0.5*10^17;
scale2 = 0.5*10^19;

regIn = zeros(1,order+2);
regOut = regIn;
w = zeros(1,order+1);
v = w;
FIR = zeros(1,length(refData)+order-1);

for i = 1:length(FIR)
    %First we update the incoming data to be the next sample
    if i <= length(refData)
        regIn(1) = refData(i);
    else
        regIn(1) = 0;
    end
    
    %Then we clock the system
    regOut = regIn;

    %When the registers update the system calculates the new values
    v(1) = regOut(1);
    w(1) = regOut(1);
    for ii = 2:order+1
        %In the lattice WFIR structure, each w node is equal to 
        if i < change*4
            v(ii) = v(ii-1) + k1(ii-1)*regOut(ii);
            w(ii) = regOut(ii)+(regOut(ii+1)-w(ii-1))*lambda + k1(ii-1)*v(ii-1);
        else
            v(ii) = v(ii-1) + k2(ii-1)*regOut(ii);
            w(ii) = regOut(ii)+(regOut(ii+1)-w(ii-1))*lambda + k2(ii-1)*v(ii-1);
        end
    end

    if i < change*4
        FIR(i) = v(end)/scale1;
    else
        FIR(i) = v(end)/scale2;
    end
        
    %When the system is stable the register inputs are ready for the next cycle
    regIn = [regIn(1), w];
end
FIRk = FIR;

save 'Test Data/fircw.mat' FIRc
save 'Test Data/firtw.mat' FIRt


%
%% Plots
% Filtering with single coefficient sets for comparison
for i = 1:length(FIR)
    %First we update the incoming data to be the next sample
    if i <= length(refData)
        regIn(1) = refData(i);
    else
        regIn(1) = 0;
    end
    
    %Then we clock the system
    regOut = regIn;

    %When the registers update the system calculates the new values
    v(1) = regOut(1);
    w(1) = regOut(1);
    for ii = 2:order+1
        %In the lattice WFIR structure, each w node is equal to 
        v(ii) = v(ii-1) + k1(ii-1)*regOut(ii);
        w(ii) = regOut(ii)+(regOut(ii+1)-w(ii-1))*lambda + k1(ii-1)*v(ii-1);
    end

    FIR(i) = v(end)/scale1;
        
    %When the system is stable the register inputs are ready for the next cycle
    regIn = [regIn(1), w];
end
FIRk1 = FIR;

for i = 1:length(FIR)
    %First we update the incoming data to be the next sample
    if i <= length(refData)
        regIn(1) = refData(i);
    else
        regIn(1) = 0;
    end
    
    %Then we clock the system
    regOut = regIn;

    %When the registers update the system calculates the new values
    v(1) = regOut(1);
    w(1) = regOut(1);
    for ii = 2:order+1
        %In the lattice WFIR structure, each w node is equal to 
        v(ii) = v(ii-1) + k1(ii-1)*regOut(ii);
        w(ii) = regOut(ii)+(regOut(ii+1)-w(ii-1))*lambda + k2(ii-1)*v(ii-1);
    end

    FIR(i) = v(end)/scale2;
        
    %When the system is stable the register inputs are ready for the next cycle
    regIn = [regIn(1), w];
end
FIRk2 =  FIR;



impres1 = b1;
impres2 = b2;

regIn = zeros(1,order+1);
regOut = regIn;
w = zeros(1,order);
FIR = zeros(1,length(refData)+order-1);

for i = 1:length(FIR)
    %First we update the incoming data to be the next sample
    if i <= length(refData)
        regIn(1) = refData(i);
    else
        regIn(1) = 0;
    end
    
    %Then we clock the system
    regOut = regIn;

    %When the registers update the system calculates the new values
    w(1) = regOut(1);
    out = w(1)*impres1(1);
    for ii = 2:order
        %In the canonical WFIR structure, each w node is equal to:
        w(ii) = regOut(ii)+(regOut(ii+1)-w(ii-1))*lambda;
        
        %And then we sum up all w nodes multiplied with their corresponding filter coefficient
        out = out + w(ii)*impres1(ii);
    end
    FIR(i) = out;
    
    %When the system is stable the register inputs are ready for the next cycle
    regIn = [regIn(1), w(1:order)];
end
FIRc1 = FIR;

regIn = zeros(1,order+1);
regOut = regIn;
w = zeros(1,order);
FIR = zeros(1,length(refData)+order-1);

for i = 1:length(FIR)
    %First we update the incoming data to be the next sample
    if i <= length(refData)
        regIn(1) = refData(i);
    else
        regIn(1) = 0;
    end
    
    %Then we clock the system
    regOut = regIn;

    %When the registers update the system calculates the new values
    w(1) = regOut(1);
    out = w(1)*impres1(1);
    for ii = 2:order
        %In the canonical WFIR structure, each w node is equal to:
        w(ii) = regOut(ii)+(regOut(ii+1)-w(ii-1))*lambda;
        
        %And then we sum up all w nodes multiplied with their corresponding filter coefficient
        out = out + w(ii)*impres2(ii);
    end
    FIR(i) = out;
    
    %When the system is stable the register inputs are ready for the next cycle
    regIn = [regIn(1), w(1:order)];
end
FIRc2 = FIR;

regIn = zeros(1,order+1);
regOut = regIn;
w = zeros(1,order);
FIR = zeros(1,length(refData)+order-1);

for i = 1:length(FIR)
    %First we update the incoming data to be the next sample
    if i <= length(refData)
        regIn(1) = refData(i);
    else
        regIn(1) = 0;
    end
    
    %Then we clock the system
    regOut = regIn;

    %When the registers update the system calculates the new values
    for ii = 1:order
        %In the transposed WFIR structure, each w node is equal to the sum of the reg before it and the inverse-corresponding coefficient multiplied with regOut(1)
        w(ii) = regOut(1)*impres1(order - ii + 1);

        if ii > 1
            w(ii) = w(ii) + regOut(ii)+(regOut(ii+1)-w(ii-1))*lambda;
        end
    end
    FIR(i) = w(end);
    
    %When the system is stable the register inputs are ready for the next cycle
    regIn = [regIn(1), w];
end
FIRt1 = FIR;

regIn = zeros(1,order+1);
regOut = regIn;
w = zeros(1,order);
FIR = zeros(1,length(refData)+order-1);

for i = 1:length(FIR)
    %First we update the incoming data to be the next sample
    if i <= length(refData)
        regIn(1) = refData(i);
    else
        regIn(1) = 0;
    end
    
    %Then we clock the system
    regOut = regIn;

    %When the registers update the system calculates the new values
    for ii = 1:order
        %In the transposed WFIR structure, each w node is equal to the sum of the reg before it and the inverse-corresponding coefficient multiplied with regOut(1)
        w(ii) = regOut(1)*impres2(order - ii + 1);

        if ii > 1
            w(ii) = w(ii) + regOut(ii)+(regOut(ii+1)-w(ii-1))*lambda;
        end
    end
    FIR(i) = w(end);
    
    %When the system is stable the register inputs are ready for the next cycle
    regIn = [regIn(1), w];
end
FIRt2 = FIR;


if change*4 < length(FIRc1)
    FIRC = [FIRc1(1:4*change-1) FIRc2(4*change:end)];
else
    FIRC = FIRc1;
end
FIRC = FIRC - FIRc;

if change*4 < length(FIRt1)
    FIRT = [FIRt1(1:4*change-1) FIRt2(4*change:end)];
else
    FIRT = FIRt1;
end
FIRT = FIRT - FIRt;


%Coefficient change marker for graphs
changeMarkX = int32(ones(1,2))*4*change;
changeMarkY = [1, -1];



figure(1)

subplot(6,1,1)
hold on;
plot(refData)
plot(FIRc)
hold off;
ylim([-4 4])
grid on;
legend('Original Signal','Filtered Signal')
title('Filtered Signal With Coefficient Change - Canonical WFIR Architecture')
xlabel('Sample')
ylabel('Amplitude')

subplot(6,1,2)
hold on;
plot(changeMarkX,changeMarkY*4, '--', 'Color', [0 0 0])
plot(FIRC)
hold off;
ylim([-4 4])
grid on;
legend('Filter Coefficient Change Mark','Filtered Signal Difference from Ideal')
title('Filtered Signal at Change Mark - Canonical WFIR Architecture')
xlabel('Sample')
ylabel('Amplitude')

subplot(6,1,3)
hold on;
plot(refData)
plot(FIRt)
hold off;
ylim([-4 4])
grid on;
legend('Original Signal','Filtered Signal')
title('Filtered Signal With Coefficient Change - Transposed WFIR Architecture')
xlabel('Sample')
ylabel('Amplitude')

subplot(6,1,4)
hold on;
plot(changeMarkX,changeMarkY*20, '--', 'Color', [0 0 0])
plot(FIRT)
hold off;
ylim([-20 20])
grid on;
legend('Filter Coefficient Change Mark','Filtered Signal Difference from Ideal')
title('Filtered Signal at Change Mark - Transposed WFIR Architecture')
xlabel('Sample')
ylabel('Amplitude')

subplot(6,1,5)
hold on;
plot(refData+10)
plot(FIRk-10)
plot(changeMarkX,changeMarkY*20, '--', 'Color', [0 0 0])
hold off;
ylim([-20 20])
grid on;
legend('Original Signal[+10]','Filtered Signal[-10]')
title('Filtered Signal With Coefficient Change - Lattice WFIR Architecture')
xlabel('Sample')
ylabel('Amplitude')

subplot(6,1,6)
hold on;
plot(FIRk1+10)
plot(FIRk2-7.5)
hold off;
ylim([-20 20])
grid on;
legend('Coefficients 1 [+10]','Coefficients 2 [-7.5]')
title('Signal Filtered With Single Sets of Coefficients in Lattice WFIR Architecture')
xlabel('Sample')
ylabel('Amplitude')

set(gcf,'position',[10,10,900,1000])
saveas(gcf, 'WFIR Coefficient Change Noise Graph.png')



figure(2)

subplot(4,1,1)
data = refData;
L = length(data);
FFT = fft(data);
FFT = fftshift(FFT);
freq = (-L/2:L/2-1)*(Fs/L);
mag = mag2db((abs(FFT).^2)/L);
plot(freq(ceil(length(freq)/2)+1:end),mag(ceil(length(freq)/2)+1:end))
ylim([-600 200])
xlim([0 Fs/2])
xticks(0:4000:Fs/2)
xticklabels(string(0:4:Fs/2000))
grid on;
title('FFT of Original Signal')
xlabel('Frequency [kHz]')
ylabel('Magnitude [dB]')

subplot(4,1,2)
hold on;
data = FIRc(order:end-order);
L = length(data);
FFT = fft(data);
FFT = fftshift(FFT);
freq = (-L/2:L/2-1)*(Fs/L);
mag = mag2db((abs(FFT).^2)/L);
plot(freq(ceil(length(freq)/2)+1:end),mag(ceil(length(freq)/2)+1:end))
data = FIRc(order:4*change);
L = length(data);
FFT = fft(data);
FFT = fftshift(FFT);
freq = (-L/2:L/2-1)*(Fs/L);
mag = mag2db((abs(FFT).^2)/L);
plot(freq(ceil(length(freq)/2)+1:end),mag(ceil(length(freq)/2)+1:end))
replot1 = freq(ceil(length(freq)/2)+1:end);
replot2 = mag(ceil(length(freq)/2)+1:end);
data = FIRc(4*change+order:end-order);
L = length(data);
FFT = fft(data);
FFT = fftshift(FFT);
freq = (-L/2:L/2-1)*(Fs/L);
mag = mag2db((abs(FFT).^2)/L);
plot(freq(ceil(length(freq)/2)+1:end),mag(ceil(length(freq)/2)+1:end), "Color", [0 0.7 0.2])
plot(replot1(floor(length(replot1)*0.4):floor(length(replot1)*0.6)),replot2(floor(length(replot2)*0.4):floor(length(replot2)*0.6)), "Color", [0.8500 0.3250 0.0980])
hold off;
ylim([-200 200])
xlim([0 Fs/2])
xticks(0:4000:Fs/2)
xticklabels(string(0:4:Fs/2000))
grid on;
legend('Signal With Change','Before Change','After Change')
title('FFT of Filtered Signal - Canonical WFIR Architecture')
xlabel('Frequency [kHz]')
ylabel('Magnitude [dB]')

subplot(4,1,3)
hold on;
data = FIRt(order:end-order);
L = length(data);
FFT = fft(data);
FFT = fftshift(FFT);
freq = (-L/2:L/2-1)*(Fs/L);
mag = mag2db((abs(FFT).^2)/L);
plot(freq(ceil(length(freq)/2)+1:end),mag(ceil(length(freq)/2)+1:end))
data = FIRt(order:4*change);
L = length(data);
FFT = fft(data);
FFT = fftshift(FFT);
freq = (-L/2:L/2-1)*(Fs/L);
mag = mag2db((abs(FFT).^2)/L);
plot(freq(ceil(length(freq)/2)+1:end),mag(ceil(length(freq)/2)+1:end))
replot1 = freq(ceil(length(freq)/2)+1:end);
replot2 = mag(ceil(length(freq)/2)+1:end);
data = FIRt(4*change+order:end-order);
L = length(data);
FFT = fft(data);
FFT = fftshift(FFT);
freq = (-L/2:L/2-1)*(Fs/L);
mag = mag2db((abs(FFT).^2)/L);
plot(freq(ceil(length(freq)/2)+1:end),mag(ceil(length(freq)/2)+1:end), "Color", [0 0.7 0.2])
plot(replot1(floor(length(replot1)*0.4):floor(length(replot1)*0.6)),replot2(floor(length(replot2)*0.4):floor(length(replot2)*0.6)), "Color", [0.8500 0.3250 0.0980])
hold off;
ylim([-200 200])
xlim([0 Fs/2])
xticks(0:4000:Fs/2)
xticklabels(string(0:4:Fs/2000))
grid on;
legend('Signal With Change','Before Change','After Change')
title('FFT of Filtered Signal - Transposed WFIR Architecture')
xlabel('Frequency [kHz]')
ylabel('Magnitude [dB]')

subplot(4,1,4)
hold on;
data = FIRk(order:end-order);
L = length(data);
FFT = fft(data);
FFT = fftshift(FFT);
freq = (-L/2:L/2-1)*(Fs/L);
mag = mag2db((abs(FFT).^2)/L);
plot(freq(ceil(length(freq)/2)+1:end),mag(ceil(length(freq)/2)+1:end))
data = FIRk(order:4*change);
L = length(data);
FFT = fft(data);
FFT = fftshift(FFT);
freq = (-L/2:L/2-1)*(Fs/L);
mag = mag2db((abs(FFT).^2)/L);
plot(freq(ceil(length(freq)/2)+1:end),mag(ceil(length(freq)/2)+1:end))
replot1 = freq(ceil(length(freq)/2)+1:end);
replot2 = mag(ceil(length(freq)/2)+1:end);
data = FIRk(4*change+order:end-order);
L = length(data);
FFT = fft(data);
FFT = fftshift(FFT);
freq = (-L/2:L/2-1)*(Fs/L);
mag = mag2db((abs(FFT).^2)/L);
plot(freq(ceil(length(freq)/2)+1:end),mag(ceil(length(freq)/2)+1:end), "Color", [0 0.7 0.2])
plot(replot1(floor(length(replot1)*0.4):floor(length(replot1)*0.6)),replot2(floor(length(replot2)*0.4):floor(length(replot2)*0.6)), "Color", [0.8500 0.3250 0.0980])
hold off;
ylim([-200 200])
xlim([0 Fs/2])
xticks(0:4000:Fs/2)
xticklabels(string(0:4:Fs/2000))
grid on;
legend('Signal With Change','Before Change','After Change')
title('FFT of Filtered Signal - Lattice WFIR Architecture')
xlabel('Frequency [kHz]')
ylabel('Magnitude [dB]')

set(gcf,'position',[600,10,1200,1000])
saveas(gcf, 'WFIR Coefficient Change FFTs.png')



figure(3)

load 'Test Data/imp1.mat'
load 'Test Data/imp2.mat'

subplot(2,1,1)
hold on;

data = FIRc(order:4*change);
L = length(data);
FFT = fft(data);
FFT = fftshift(FFT);
freq = (-L/2:L/2-1)*(Fs/L);
mag = mag2db((abs(FFT).^2)/L);
plot(freq(ceil(length(freq)/2)+1:end),mag(ceil(length(freq)/2)+1:end))

data = impres1;
L = length(data);
FFT = fft(data);
FFT = fftshift(FFT);
freq = (-L/2:L/2-1)*(Fs/L);
mag = mag2db((abs(FFT).^2)/L);
plot(freq(ceil(length(freq)/2)+1:end),mag(ceil(length(freq)/2)+1:end),'LineWidth',1.25)

plot(points1(1,:)*Fs/2,mag2db(points1(2,:)+10^-9)-100,'LineWidth',1.25)

hold off;
xlim([0 Fs/2])
xticks(0:4000:Fs/2)
xticklabels(string(0:4:Fs/2000))
ylim([-300 300])
grid on;
legend('Warped Response','Standard Response', 'Sketched Response[-100 dB]','FontSize',12)
title('Frequency Responses - Coefficient Set 1','FontSize',14)
xlabel('Frequency [kHz]','FontSize',12)
ylabel('Magnitude [dB]','FontSize',12)

subplot(2,1,2)
hold on;

data = FIRc(4*change+order:end-order*2);
L = length(data);
FFT = fft(data);
FFT = fftshift(FFT);
freq = (-L/2:L/2-1)*(Fs/L);
mag = mag2db((abs(FFT).^2)/L);
plot(freq(ceil(length(freq)/2)+1:end),mag(ceil(length(freq)/2)+1:end))

data = impres2;
L = length(data);
FFT = fft(data);
FFT = fftshift(FFT);
freq = (-L/2:L/2-1)*(Fs/L);
mag = mag2db((abs(FFT).^2)/L);
plot(freq(ceil(length(freq)/2)+1:end),mag(ceil(length(freq)/2)+1:end),'LineWidth',1.25)

plot(points2(1,:)*Fs/2,mag2db(points2(2,:)+10^-9)-100,'LineWidth',1.25)

hold off;
xlim([0 Fs/2])
xticks(0:4000:Fs/2)
xticklabels(string(0:4:Fs/2000))
ylim([-300 300])
grid on;
legend('Warped Response','Standard Response', 'Sketched Response[-100 dB]','FontSize',12)
title('Frequency Responses - Coefficient Set 2','FontSize',14)
xlabel('Frequency [kHz]','FontSize',12)
ylabel('Magnitude [dB]','FontSize',12)

set(gcf,'position',[10,10,1800,800])
saveas(gcf, 'WFIR Filter Response Comparison.png')

%}