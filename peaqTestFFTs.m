%FFTs for comparison with tests of GstPEAQ implementation

clear
close all

%% Setup
samples = 1000;
points = [0 3 10 50 200 800];

dim = length(points);

%% Piano
refFile = 'gstpeaq-develop github\vs\x64\Release\sounds\piano.wav';
[refData,Fs] = audioread(refFile);

L = length(refData);
freq1 = (-L/2:L/2-1)*(Fs/L);

data1 = [];
for i = 1:dim
    for j = 1:points(i)
        if j > 1
            for k = 0:floor(L/(samples*8))
                refData(floor((j-1)*L/samples)+k) = rand();
            end
        end
    end
    data1 = [data1, refData];
end


%% Rock
refFile = 'gstpeaq-develop github\vs\x64\Release\sounds\rock.wav';
[refData,Fs] = audioread(refFile);

L = length(refData);
freq2 = (-L/2:L/2-1)*(Fs/L);

data2 = [];
for i = 1:dim
    for j = 1:points(i)
        if j > 1
            for k = 0:floor(L/(samples*8))
                refData(floor((j-1)*L/samples)+k) = rand();
            end
        end
    end
    data2 = [data2, refData];
end


%% Orchestra
refFile = 'gstpeaq-develop github\vs\x64\Release\sounds\orchestra.wav';
[refData,Fs] = audioread(refFile);

L = length(refData);
freq3 = (-L/2:L/2-1)*(Fs/L);

data3 = [];
for i = 1:dim
    for j = 1:points(i)
        if j > 1
            for k = 0:floor(L/(samples*8))
                refData(floor((j-1)*L/samples)+k) = rand();
            end
        end
    end
    data3 = [data3, refData];
end


%% Nothing
Fs = 48000;
refData = zeros(Fs*30,1);

L = length(refData);
freq4 = (-L/2:L/2-1)*(Fs/L);

data4 = [];
for i = 1:dim
    for j = 1:points(i)
        if j > 1
            for k = 0:floor(L/(samples*8))
                refData(floor((j-1)*L/samples)+k) = rand();
            end
        end
    end
    data4 = [data4, refData];
end


%% Plot

figure(1);

for i = 1:dim
    subplot(4,dim,dim*0+i)
    plot(freq1, mag2db(abs(fftshift(fft(data1(:,i))))))
    xlim([-1, 1]*Fs/2)
    ylim([-100, 100])
    grid on
    xlabel('Frequency [Hz]')
    ylabel('Magnitude [dB]')
    title(strcat("Piano Music - sample ", num2str(points(i))))

    subplot(4,dim,dim*1+i)
    plot(freq2, mag2db(abs(fftshift(fft(data2(:,i))))))
    xlim([-1, 1]*Fs/2)
    ylim([-100, 100])
    grid on
    xlabel('Frequency [Hz]')
    ylabel('Magnitude [dB]')
    title(strcat("Rock Music - sample ", num2str(points(i))))
    
    subplot(4,dim,dim*2+i)
    plot(freq3, mag2db(abs(fftshift(fft(data3(:,i))))))
    xlim([-1, 1]*Fs/2)
    ylim([-100, 100])
    grid on
    xlabel('Frequency [Hz]')
    ylabel('Magnitude [dB]')
    title(strcat("Orchestra Music - sample ", num2str(points(i))))
    
    subplot(4,dim,dim*3+i)
    plot(freq4, mag2db(abs(fftshift(fft(data4(:,i))))))
    xlim([-1, 1]*Fs/2)
    ylim([-100, 100])
    grid on
    xlabel('Frequency [Hz]')
    ylabel('Magnitude [dB]')
    title(strcat("The Added Noise - sample ", num2str(points(i))))
end

set(gcf,'position',[10,10,1800,1200])
saveas(gcf, "PEAQ Noise FFTs - GND.png")

%}