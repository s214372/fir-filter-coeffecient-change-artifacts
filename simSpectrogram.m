%Filtering hardware simulations with pulse-based coefficient changes

close all force
clear

fprintf('Start Time: %s\n', datetime("now"));

%% Basic Setup
scale1 = 16;
scale2 = 8;
order = 256;
fileLength = 56000;
change = int32(0x40);
lenScale = 1000;


%Create tone complex signal
Freqs = [5000 12000 19000];
Fs = 48000;
Ts = fileLength/Fs;

TimeVector = 1/Fs:1/Fs:Ts;
Signal = sin(2*pi*Freqs(1)*TimeVector);
for i = 2:length(Freqs)
    Signal = Signal + sin(2*pi*Freqs(i)*TimeVector);
end
refData = Signal;

if length(refData) > 0x800
    change = change*int32(0x80);
end
%}



%% Standard FIR Setup
warped = 0;

%Generate impulse responses for filtering
run('generateFIR.m')
impres1 = b1;
impres2 = b1*32;


%% Hardware Simulation Loops
fprintf('\nStarting Simulation Loops...\n');

%Canonical FIR
regIn = zeros(1,order);
regOut = regIn;
w = regOut;
FIR = zeros(1,length(refData)+order-1);

for i = 1:length(FIR)
    %First we update the incoming data to be the next sample
    if i <= length(refData)
        regIn(1) = refData(i);
    else
        regIn(1) = 0;
    end
    
    %Then we clock the system
    regOut = regIn;

    %When the registers update the system calculates the new values
    out = 0;
    for ii = 1:order
        %In the canonical FIR structure, each w node is simply equal to the corresponding register output
        w(ii) = regOut(ii);
        
        %And then we sum up all w nodes multiplied with their corresponding filter coefficient
        if i < change*4 || i > change*4+Fs/lenScale
            out = out + w(ii)*impres1(ii);
        else
            out = out + w(ii)*impres2(ii);
        end
    end
    FIR(i) = out;
    
    %When the system is stable the register inputs are ready for the next cycle
    regIn = [regIn(1), w(1:order-1)];
end
FIRcn = FIR;
fprintf('Canonical FIR Complete at: %s\n', datetime("now"));

%Transposed FIR
regIn = zeros(1,order);
regOut = regIn;
w = regOut;
FIR = zeros(1,length(refData)+order-1);

for i = 1:length(FIR)
    %First we update the incoming data to be the next sample
    if i <= length(refData)
        regIn(1) = refData(i);
    else
        regIn(1) = 0;
    end
    
    %Then we clock the system
    regOut = regIn;

    %When the registers update the system calculates the new values
    for ii = 1:order
        %In the transposed FIR structure, each w node is equal to the sum of the reg before it and the inverse-corresponding coefficient multiplied with regOut(1)
        if i < change*4 || i > change*4+Fs/lenScale
            w(ii) = regOut(1)*impres1(order - ii + 1);
        else
            w(ii) = regOut(1)*impres2(order - ii + 1);
        end

        if ii > 1
            w(ii) = w(ii) + regOut(ii);
        end
    end
    FIR(i) = w(end);
    
    %When the system is stable the register inputs are ready for the next cycle
    regIn = [regIn(1), w(1:order-1)];
end
FIRtn = FIR;
fprintf('Transposed FIR Complete at: %s\n', datetime("now"));



%% Warped FIR Setup
warped = 1;
lambda = 0.75;
scale1 = 16;
scale2 = 56;

run('generateFIR.m')

impres1 = b1;
impres2 = b1*32;


%% Hardware Simulation Loops

%Canonical WFIR
regIn = zeros(1,order+1);
regOut = regIn;
w = zeros(1,order);
FIR = zeros(1,length(refData)+order-1);

for i = 1:length(FIR)
    %First we update the incoming data to be the next sample
    if i <= length(refData)
        regIn(1) = refData(i);
    else
        regIn(1) = 0;
    end
    
    %Then we clock the system
    regOut = regIn;

    %When the registers update the system calculates the new values
    w(1) = regOut(1);
    out = w(1)*impres1(1);
    for ii = 2:order
        %In the canonical WFIR structure, each w node is equal to:
        w(ii) = regOut(ii)+(regOut(ii+1)-w(ii-1))*lambda;
        
        %And then we sum up all w nodes multiplied with their corresponding filter coefficient
        if i < change*4 || i > change*4+Fs/lenScale
            out = out + w(ii)*impres1(ii);
        else
            out = out + w(ii)*impres2(ii);
        end
    end
    FIR(i) = out;
    
    %When the system is stable the register inputs are ready for the next cycle
    regIn = [regIn(1), w(1:order)];
end
FIRcw = FIR;
fprintf('Canonical WFIR Complete at: %s\n', datetime("now"));

%Transposed WFIR
regIn = zeros(1,order+1);
regOut = regIn;
w = zeros(1,order);
FIR = zeros(1,length(refData)+order-1);

for i = 1:length(FIR)
    %First we update the incoming data to be the next sample
    if i <= length(refData)
        regIn(1) = refData(i);
    else
        regIn(1) = 0;
    end
    
    %Then we clock the system
    regOut = regIn;

    %When the registers update the system calculates the new values
    for ii = 1:order
        %In the transposed WFIR structure, each w node is equal to the sum of the reg before it and the inverse-corresponding coefficient multiplied with regOut(1)
        if i < change*4 || i > change*4+Fs/lenScale
            w(ii) = regOut(1)*impres1(order - ii + 1);
        else
            w(ii) = regOut(1)*impres2(order - ii + 1);
        end

        if ii > 1
            w(ii) = w(ii) + regOut(ii)+(regOut(ii+1)-w(ii-1))*lambda;
        end
    end
    FIR(i) = w(end);
    
    %When the system is stable the register inputs are ready for the next cycle
    regIn = [regIn(1), w];
end
FIRtw = FIR;
fprintf('Transposed WFIR Complete at: %s\n', datetime("now"));


%
%% Plots
fprintf('\nStarting First Plot...\n');

figure(1)
L = fileLength + order - 1;
freq = (-L/2:L/2-1)*(Fs/L);
pulse1 = [zeros(1,change*4) ones(1,Fs/lenScale) zeros(1,fileLength-(change*4+Fs/lenScale)+order-1)];
pulse2 = [zeros(1,change*4) ones(1,fileLength-change*4+order-1)];

subplot(9,6,[16 24])
plot(freq, mag2db(abs(fftshift(fft(pulse1)))),'LineWidth',1.5)
ylim([-100 100])
xlim([-Fs/2 Fs/2])
xticks(-Fs/2:4000:Fs/2)
xticklabels(string(-Fs/2000:4:Fs/2000))
grid on;
f = gca;
f.LineWidth = 2;
f.GridLineWidth = 1;
f.FontSize = 20;
xlabel('Frequency [kHz]','FontSize',20)
ylabel('Magnitude [dB]','FontSize',20)
title('Pulse Signal - Frequency Domain','FontSize',24)

subplot(9,6,[40 48])
plot(freq, mag2db(abs(fftshift(fft(pulse2)))),'LineWidth',1.5)
ylim([-100 100])
xlim([-Fs/2 Fs/2])
xticks(-Fs/2:4000:Fs/2)
xticklabels(string(-Fs/2000:4:Fs/2000))
grid on;
f = gca;
f.LineWidth = 2;
f.GridLineWidth = 1;
f.FontSize = 20;
xlabel('Frequency [kHz]','FontSize',20)
ylabel('Magnitude [dB]','FontSize',20)
title('Step Signal - Frequency Domain','FontSize',24)

subplot(9,6,[19 44])
spectrogram(FIRcn, 2048, 1024, 2048, Fs, 'yaxis')
xlabel('Time [s]','FontSize',20)
ylabel('Frequency [kHz]','FontSize',20)
f = gca;
f.FontSize = 20;
f.LineWidth = 2;
c = colorbar;
c.Label.String = 'Power/Frequency [dB/Hz]';
c.Label.FontSize = 20;
c.LineWidth = 2;
title('Pulse Coefficient Change','FontSize',24)

set(gcf,'position',[10,10,1800,700])
saveas(gcf, "Pulse Plots.png")



figure(2)
ref = [FIRcn; FIRtn; FIRcw; FIRtw];
labels = ["Canonical FIR", "Transposed FIR", "Canonical WFIR", "Transposed WFIR"];

for j = 1:4
    subplot(4,4,[j*4-3 j*4-1])
    hold on;
    data = ref(j,order:end-order);
    L = length(data);
    FFT = fft(data);
    FFT = fftshift(FFT);
    freq = (-L/2:L/2-1)*(Fs/L);
    mag = mag2db((abs(FFT).^2)/L);
    plot(freq(ceil(length(freq)/2)+1:end),mag(ceil(length(freq)/2)+1:end))
    data = ref(j,order:4*change-order);
    L = length(data);
    FFT = fft(data);
    FFT = fftshift(FFT);
    freq = (-L/2:L/2-1)*(Fs/L);
    mag = mag2db((abs(FFT).^2)/L);
    plot(freq(ceil(length(freq)/2)+1:end),mag(ceil(length(freq)/2)+1:end))
    replot1 = freq(ceil(length(freq)/2)+1:end);
    replot2 = mag(ceil(length(freq)/2)+1:end);
    data = ref(j,4*change+order:end-order);
    L = length(data);
    FFT = fft(data);
    FFT = fftshift(FFT);
    freq = (-L/2:L/2-1)*(Fs/L);
    mag = mag2db((abs(FFT).^2)/L);
    plot(freq(ceil(length(freq)/2)+1:end),mag(ceil(length(freq)/2)+1:end), "Color", [0 0.7 0.2])
    plot(replot1(floor(length(replot1)*0.4):floor(length(replot1)*0.6)),replot2(floor(length(replot2)*0.4):floor(length(replot2)*0.6)), "Color", [0.8500 0.3250 0.0980])
    hold off;
    ylim([-200 200])
    xlim([0 Fs/2])
    xticks(0:4000:Fs/2)
    xticklabels(string(0:4:Fs/2000))
    grid on;
    legend('Signal With Change','Before Change','After Change')
    title(strcat("FFT of Filtered Signal - ", labels(j)))
    xlabel('Frequency [kHz]')
    ylabel('Magnitude [dB]')

    subplot(4,4,j*4)
    spectrogram(ref(j,:), 2048, 1024, 2048, Fs, 'yaxis')
    title("Spectrogram")
end

set(gcf,'position',[10,10,1800,2000])
saveas(gcf, "Pulse-Signal Spectrograms.png")


fprintf('Plots Complete at: %s\n', datetime("now"));

%}