%(Non-Script Version of peaqScript) runs peaq script on a ref and test file

clear

%% Call peaq script version
command = "powershell;./'gstpeaq-develop github\vs\x64\Release\peaq' --version";
[~,cmdout] = system(command);

%Identify version text in cmdout as initial text up until "Copyright "
version = '';
compStr = 'Copyright ';
for i = 10:(length(cmdout)-length(compStr))
  if strcmp(cmdout(i:(i+length(compStr)-1)),compStr)
    version = cmdout(1:i-2);
    break
  end
end

%Assert error if wrong version
if not(strcmp(version,'GstPEAQ 0.6.1'))
    error('Version Mismatch');
end

%% Call basic peaq script
command = "powershell;./'gstpeaq-develop github\vs\x64\Release\peaq' --basic";
refFile = 'gstpeaq-develop github\vs\x64\Release\sounds\note.wav';
testFile = 'gstpeaq-develop github\vs\x64\Release\sounds\note.wav';
[~,cmdout] = system(append(command," '",refFile,"' '",testFile,"'"));

%Identify doubles in cmdout from string fields other than the initial
%"Objective Difference Grade: " and middle "Distortion Index: " strings
ODG = NaN;
DI = NaN;
compStr = 'Distortion Index: ';
for i = 32:(length(cmdout)-length(compStr))
  if strcmp(cmdout(i:(i+length(compStr)-1)),compStr)
    ODG = str2double(cmdout(29:i-2));
    DI = str2double(cmdout(i+length(compStr):end));
    break
  end
end

%Print peaq values
objectiveDifferenceGrade = ODG
distortionIndex = DI
